﻿namespace JLAuto.Enums
{
    public enum TimeUnit : byte
    {
        Milliseconds = 0,
        Seconds = 1,
        Minutes = 2,
    }
}
