﻿namespace JLAuto.Enums
{
    public enum BrowserType
    {
        Chrome,
        Firefox,
        Edge
    }
}
