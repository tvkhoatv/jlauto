﻿namespace JLAuto.Enums
{
    public enum ToastNotificationType
    {
        Information,
        Success,
        Warning,
        Error,
    }
}
