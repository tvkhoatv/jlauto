﻿using Bogus;

namespace Infrastructure.Utils
{
    public class FakeData : Faker
    {
        public FakeData() : base()
        {
            Locale = "en";
        }
    }
}
