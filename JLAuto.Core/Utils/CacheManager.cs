﻿using log4net;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace JLAuto.Core.Utils
{
    public static class CacheManager
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static bool EnableCache = true;
        public static MemoryCache Cache { get; set; }
        static CacheManager()
        {
            Cache = new MemoryCache(new MemoryCacheOptions());
        }

        public static void Add<T>(string key, T o)
        {
            try
            {
                if (CacheEnable())
                {
                    if (!Exists(key))
                    {
                        //var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromHours(24));
                        Cache.Set(key, o);
                        Logger.DebugFormat("Key : {0} inserted", key);

                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Fatal($"key:{key},exception:{ex.Message}");
            }
        }

        private static List<string> GetAllKeysList()
        {
            var field = typeof(MemoryCache).GetProperty("EntriesCollection", BindingFlags.NonPublic | BindingFlags.Instance);
            var collection = field?.GetValue(Cache) as ICollection;
            var items = new List<string>();
            if (collection != null)
                foreach (var item in collection)
                {
                    var methodInfo = item?.GetType().GetProperty("Key");
                    var val = methodInfo?.GetValue(item);
                    items.Add(val.ToString());
                }
            return items;
        }

        private static bool CacheEnable()
        {
            return EnableCache;
        }

        public static bool Clear(string key)
        {
            var keyDeleted = false;
            if (CacheEnable())
            {
                try
                {
                    if (Exists(key))
                    {
                        Cache.Remove(key);
                        Logger.InfoFormat("Key : {0} cleared", key);
                        keyDeleted = true;
                    }
                    else
                        Logger.InfoFormat("key : {0} not found", key);
                }
                catch (Exception ex)
                {
                    Logger.Fatal($"key:{key},exception:{ex.Message}");
                }
            }
            return keyDeleted;
        }

        public static bool Exists(string key)
        {
            bool result = false;
            if (Cache.TryGetValue(key, out var _))
            {
                result = true;
            }
            return result;
        }

        public static T Get<T>(string key)
        {
            try
            {
                if (CacheEnable())
                {
                    if (Cache.TryGetValue(key, out T result))
                    {
                        return result;
                    }
                    Logger.DebugFormat("Key : {0} does not exists", key);
                }
                return default;
            }
            catch (Exception ex)
            {
                Logger.Fatal($"key:{key},exception:{ex.Message}");
                return default;
            }
        }

        public static bool ClearFromTablePrefix(string key)
        {
            var keyDeleted = false;
            if (CacheEnable())
            {
                var cacheKeys = GetAllKeysList();
                var cacheItem = new List<string>();
                foreach (var cacheKey in cacheKeys)
                {
                    if (cacheKey.StartsWith(key))
                        cacheItem.Add(cacheKey);
                }
                Logger.DebugFormat("Number of Key(s) found {0}, with Prefix {1}", cacheItem.Count, key);

                try
                {
                    foreach (var newkey in cacheItem)
                    {
                        Cache.Remove(key);
                        Logger.DebugFormat("Key : {0} cleared", newkey);

                    }
                    keyDeleted = true;
                }
                catch (Exception ex)
                {
                    Logger.Fatal($"key:{key},exception:{ex.Message}");
                }

            }
            return keyDeleted;
        }

        public static bool RemoveAllCache()
        {
            try
            {
                if (CacheEnable())
                {
                    var cacheKeys = GetAllKeysList();
                    foreach (var cacheKey in cacheKeys)
                    {
                        Cache.Remove(cacheKey);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex.Message);
                return false;
            }
        }
    }
}
