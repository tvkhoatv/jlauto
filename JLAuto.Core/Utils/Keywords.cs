﻿using JLAuto.Factory;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using JLAuto.Infrastructure.Base;
using JLAuto.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading;
using System;
using System.Linq;

namespace JLAuto.Utils
{
    public class Keywords
    {
        #region Declare field
        private readonly IWebDriver _driver;
        private byte[] imageByte { get; set; }
        private readonly WaitHelper _waitHelper;
        protected static LockObject lockObject = new LockObject();
        #endregion

        #region Constructor
        public Keywords(DriverFactory driverFactory)
        {
            _waitHelper = new WaitHelper(driverFactory);
            _driver = driverFactory.PDriver;
        }

        //public Keywords(MobileAppFactory mobileAppFactory)
        //{
        //    _waitHelper = new WaitHelper(mobileAppFactory);
        //    _driver = mobileAppFactory.Driver;
        //}
        #endregion

        #region JS click using WebElement
        public void JSClickOn(IWebElement element)
        {
            _waitHelper.WaitForElementEnable(element);
            ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].click();", element);
        }

        public void ClickOn(IWebElement element)
        {
            _waitHelper.WaitForPageFullLoaded();
            int retry = 3;
            while (retry > 0)
            {
                Exception exeption = null;
                try
                {
                    element = _waitHelper.WaitForElementVisible(element);
                    element = _waitHelper.WaitForElementClickable(element);
                    element.Click();
                    break;
                }
                catch (StaleElementReferenceException ex)
                {
                    throw ex;
                }
                catch (ElementClickInterceptedException ex)
                {
                    if (ex.Message.Contains("toast"))
                    {
                        JSClickOn(element);
                    }
                    else
                    {
                        Thread.Sleep(1500);
                        exeption = ex;
                    }
                }
                catch (Exception ex)
                {
                    Thread.Sleep(1500);
                    exeption = ex;
                }
                finally
                {
                    //if (ex is ElementClickInterceptedException ||
                    //    ex is WebDriverException || // handle case target frame detached
                    //   (ex is TargetInvocationException && ex.InnerException.Message.Contains("click")) ||
                    //    ex is NoSuchElementException)
                    //{
                    retry--;

                    if (retry == 0)
                        throw new ArgumentException($"Retry click 3 times", exeption);
                }
            }
        }
        /// <summary>
        /// This method to dertermine the duplicated elements, we need to find out which element in list is visible to click
        /// </summary>
        /// <param name="element"></param>
        public void ClickOn(IList<IWebElement> elements)
        {
            _waitHelper.WaitForPageFullLoaded();
            //Thread.Sleep(1500);
            elements = _waitHelper.WaitForElementsVisible(elements);

            ClickOn(elements.FirstOrDefault(x => x.Displayed));
        }
        #endregion

        #region Execute javascript
        public object ExecuteJS(string command)
        {
            return ((IJavaScriptExecutor)_driver).ExecuteScript(command);
        }
        #endregion

        #region Set attribure
        public void SetTextAttribute(IWebElement element, string value)
        {
            var jsExecutor = (IJavaScriptExecutor)_driver;
            jsExecutor.ExecuteScript("arguments[0].value = arguments[1];", element, value);
        }
        #endregion

        #region Scroll to element
        public IWebElement ScrollToElementByAction(IWebElement element, int offsetX = 0, int offsetY = 0)
        {
            _waitHelper.WaitForElementVisible(element);
            Actions actions = new Actions(_driver);
            actions.MoveToElement(element, offsetX, offsetY)
                   .Build()
                   .Perform();
            _waitHelper.WaitForElementVisible(element);
            return element;
        }
        public IWebElement ScrollToElementByJS(IWebElement element)
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)_driver;
            executor.ExecuteScript("arguments[0].scrollIntoView({ block: 'center', inline: 'center' });", element);
            Thread.Sleep(1000);
            return element;
        }

        public void ScrollToPosition(int width, int heigh)
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)_driver;
            executor.ExecuteScript($"window.scrollTo({width},{heigh});");
            Thread.Sleep(1000);
        }

        public void ScrollToBottom()
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)_driver;
            executor.ExecuteScript("$('html,body').animate({scrollTop: document.body.scrollHeight});");
            Thread.Sleep(1000);
        }

        public void ScrollInModal(IWebElement modal, IWebElement element)
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)_driver;
            //int x = element.Location.X;
            int y = element.Location.Y + element.Size.Height;
            executor.ExecuteScript($"arguments[0].scrollTop={y}", modal);
            Thread.Sleep(1000);
        }

        #endregion

        #region Get Data Rows
        /// <summary>
        /// Get all text of all rows.
        /// </summary>
        /// <param name="lbl_rows">Element for all data rows</param>
        public List<List<string>> GetRows(IList<IWebElement> lbl_rows, string attribute = "textContent")
        {
            List<List<string>> rows = new List<List<string>>();
            foreach (IWebElement lbl_row in lbl_rows)
            {
                List<string> value = new List<string>();
                var tds = lbl_row.FindElements(By.TagName("td"));
                if (tds.Count > 0)
                {
                    foreach (var td in tds)
                    {
                        string text = td.GetAttribute(attribute).Trim();
                        value.Add(text);
                    }
                }
                else
                {
                    value = lbl_row.GetAttribute(attribute)
                                   .Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                                   .ToList();
                }

                rows.Add(value);
            }
            return rows;
        }
        #endregion

        #region Get index of row by text
        public int GetIndexRowByText(List<List<string>> rows, string text)
        {
            int index = -1;
            foreach (var row in rows)
            {
                foreach (var col in row)
                {
                    if (col.Contains(text))
                    {
                        index = rows.IndexOf(row);
                        break;
                    }
                }
            }
            return index;
        }
        #endregion

        #region Get text by column
        public List<string> GetTextByColumnName(IWebElement table, string text)
        {
            List<string> value = null;
            var header = table.FindElements(By.TagName("th"));

            for (int i = 0; i < header.Count(); i++)
            {
                var colName = header[i].Text;
                if (colName.Equals(text))
                {
                    int index = i + 1;
                    var colData = table.FindElements(By.XPath(".//td[" + index + "]"));
                    value = colData.Select(e => e.GetAttribute("innerText")).ToList();
                    break;
                }
            }
            return value;
        }
        #endregion

        #region Take action on checkbox
        public void DoTheCheckbox(IWebElement element, bool check)
        {
            _waitHelper.WaitForElementEnable(element);
            if (element.Selected != check)
            {
                JSClickOn(element.FindElement(By.XPath("parent::*")));
            }
        }
        #endregion

        #region Click on Action Menu button
        public void ClickOnActionButton(IWebElement actionMenu, IWebElement button)
        {
            _waitHelper.WaitForPageFullLoaded();
            if (button.Displayed)
            {
                ClickOn(button);
            }
            else
            {
                ClickOn(actionMenu);
                _waitHelper.WaitForElementVisible(button);
                ClickOn(button);
            }
        }
        #endregion

        #region Click on Action Menu button
        public void ClickOnActionButtonJS(IWebElement actionMenu, IWebElement button)
        {
            _waitHelper.WaitForPageFullLoaded();
            if (button.Displayed)
            {
                JSClickOn(button);
            }
            else
            {
                JSClickOn(actionMenu);
                _waitHelper.WaitForElementVisible(button);
                JSClickOn(button);
            }
        }
        #endregion

        #region Double click
        public void DoubleClick(IWebElement element)
        {
            Actions actions = new Actions(_driver);
            actions.DoubleClick(element).Perform();
        }
        #endregion

        #region Sendkey on custom text box
        /// <summary>
        /// This keywork to serve the dropdown that has to click on to enable the textbox
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="optionItem"></param>
        /// <param name="text"></param>
        public void SendkeysOnCustomDropdown(IWebElement textBox, IWebElement optionItem, string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                _waitHelper.WaitForPageFullLoaded();
                var parentElement = textBox.FindElement(By.XPath("parent::*"));

                //if (!string.IsNullOrEmpty(parentElement.Text))
                //{
                ClickOn(parentElement);
                //    //Wait for the list loaded
                //    _waitHelper.WaitForPageFullLoaded();
                //}

                SendkeysOnTextBox(textBox, optionItem, text);
                _waitHelper.WaitForPageFullLoaded();
                //ClickOn(parentElement);
            }
        }
        public void SendkeysOnTextBox(IWebElement textBox, IWebElement optionItem, string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                _waitHelper.WaitForElementVisible(textBox);
                //ClearText(textBox);
                SendkeysOnTextBox(textBox, text);
                Thread.Sleep(2000);
                _waitHelper.WaitForPageFullLoaded();

                _waitHelper.WaitForElementVisible(optionItem);

                Actions actions = new Actions(_driver);
                actions.MoveToElement(optionItem)
                        .Click(optionItem)
                        .Build()
                        .Perform();
            }
        }

        public void SendkeysOnTextBox(IWebElement textBox, string text)
        {
            _waitHelper.WaitForElementVisible(textBox);
            //string[] roleAttr = { "combobox", "listbox" };
            //if (roleAttr.Contains(textBox.GetAttribute("role")?.ToLower()))
            //{
            //    Thread.Sleep(2000);
            //}

            //if textbox type is search or number, using ClearTextJS
            string[] typeAttr = { "search", "number" };
            if (typeAttr.Contains(textBox.GetAttribute("type")?.ToLower()))
            {
                ClearTextJS(textBox);
                textBox.Click();
            }
            else
            {
                ClearText(textBox);
            }

            if (!string.IsNullOrEmpty(text))
            {
                textBox.SendKeys(text);
            }
        }

        public void SendkeysOnTextBox(IList<IWebElement> textBoxs, string text)
        {
            _waitHelper.WaitForPageFullLoaded();
            textBoxs = _waitHelper.WaitForElementsVisible(textBoxs);
            var textBox = textBoxs.FirstOrDefault(x => x.Displayed);
            ClearText(textBox);

            if (!string.IsNullOrEmpty(text))
            {
                //textBox.SendKeys(text);
                SendkeysOnTextBox(textBox, text);
            }
        }

        public void SendkeysOnSpanTextBox(IWebElement textBox, IWebElement optionItem, string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                _waitHelper.WaitForElementVisible(textBox);
                if (textBox.FindElement(By.XPath("parent::*")).GetAttribute("innerText") != string.Empty)
                {
                    ClickOn(textBox.FindElement(By.XPath("preceding-sibling::*")));
                }

                textBox.Clear();
                textBox.SendKeys(text);

                _waitHelper.WaitForPageFullLoaded();

                _waitHelper.WaitForElementVisible(optionItem);

                Actions actions = new Actions(_driver);
                actions.MoveToElement(optionItem)
                        .Click(optionItem)
                        .Build()
                        .Perform();
            }
        }

        public void SendKeysOnDateTimeTextBox(IWebElement textbox, DateTime? date, string format)
        {
            _waitHelper.WaitForElementVisible(textbox);
            textbox.Clear();

            if (date != null)
            {
                textbox.Click();
                Actions actions = new Actions(_driver);
                actions.KeyDown(Keys.Control).SendKeys("A").KeyUp(Keys.Control).SendKeys(Keys.Backspace).Perform();
                actions.SendKeys(date?.ToString(format)).Perform();
                actions.SendKeys(Keys.Escape).Perform();
            }
        }

        public void Sendkeys(IWebElement textBox, string text)
        {
            _waitHelper.WaitForElementVisible(textBox);

            if (!string.IsNullOrEmpty(text))
            {
                _waitHelper.WaitForElementEnable(textBox);
                ClearText(textBox);
                //textBox.Clear();
                if (textBox.Text != string.Empty ||
                    textBox.GetAttribute("innerText") != string.Empty ||
                    textBox.GetAttribute("value") != string.Empty ||
                    textBox.GetAttribute("TextContent") != string.Empty)
                {
                    ClearTextJS(textBox);
                }
                textBox.SendKeys(text);
            }
        }

        public void Sendkeys(IWebElement textBox, IList<IWebElement> optionItems, string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                Sendkeys(textBox, text);

                _waitHelper.WaitForPageFullLoaded();
                Thread.Sleep(2000);
                _waitHelper.WaitForElementsVisible(optionItems);

                text = text.Length > 30 ? text[30..] : text;
                var optionItem = optionItems.First(x => text.Contains(x.Text));

                Actions actions = new Actions(_driver);
                actions.MoveToElement(optionItem)
                        .Click(optionItem)
                        .Build()
                        .Perform();
            }
        }

        public void ClearText(IWebElement element)
        {
            //ClickOn(element);
            int retry = 3;
            while (retry > 0)
            {
                try
                {
                    element.Click();
                    element.Clear();
                    break;
                }
                catch (WebDriverException ex) // handle case target frame detached
                {
                    Thread.Sleep(1500);
                    retry--;
                    if (retry == 0)
                        throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void ClearText(IList<IWebElement> elements)
        {
            var element = elements.FirstOrDefault(x => x.Displayed);
            ClearText(element);
            //ClickOn(element);
            //element.Click();
            //element.Clear();
        }
        #endregion

        #region Sendkeys by js
        public void SendkeysJS(IWebElement element, string text)
        {
            ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].setAttribute('value', '" + text + "')", element);
        }
        #endregion

        #region Clear text by js
        public void ClearTextJS(IWebElement element)
        {
            int retry = 3;
            do
            {
                JSClickOn(element);
                ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].value = ''", element);
                retry--;
            } while (retry > 0 && element.GetAttribute("value") != string.Empty);
        }
        #endregion

        #region Select dropdown
        public void SelectDropdownByText(IWebElement element, string text)
        {
            _waitHelper.WaitForElementVisible(element);
            new SelectElement(element).SelectByText(text, true);
        }
        #endregion

        #region Select dropdown
        public void SelectDropdownByValue(IWebElement element, string value)
        {
            _waitHelper.WaitForElementVisible(element);
            if (!string.IsNullOrEmpty(value))
                new SelectElement(element).SelectByValue(value);
        }
        #endregion

        #region Get Text form dropdown
        public string GetSelectedComboboxByText(IWebElement element) => new SelectElement(element).SelectedOption.Text;
        public string GetSelectedComboboxByValue(IWebElement element) => new SelectElement(element).SelectedOption.GetAttribute("value");
        #endregion

        #region Open new tab
        public void OpenNewTab()
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)_driver;
            executor.ExecuteScript("window.open();");
        }
        #endregion

        #region Switch to tab
        public void SwitchToFirstTab()
        {
            _driver.Close();
            string window = _driver.WindowHandles.FirstOrDefault();
            _driver.SwitchTo().Window(window);
        }
        public void SwitchToLastTab()
        {
            _driver.Close();
            string window = _driver.WindowHandles.LastOrDefault();
            _driver.SwitchTo().Window(window);
        }
        #endregion

        #region Drag&Drop element
        public void DragDropElement(IWebElement draggedElement, IWebElement destinationElement)
        {
            _waitHelper.WaitForElementVisible(draggedElement);
            Actions actions = new Actions(_driver);

            //actions.DragAndDrop(draggedElement, destinationElement)
            //       .Perform();
            /*Thread.Sleep(1000);
            actions.ClickAndHold(draggedElement).Perform();
            Thread.Sleep(1000);
            actions.MoveToElement(destinationElement, 0, 5).Perform();
            Thread.Sleep(1000);
            actions.Release(destinationElement).Perform();*/

            Thread.Sleep(3000);
            actions.ClickAndHold(draggedElement).Perform();
            Thread.Sleep(1000);
            actions.MoveToElement(destinationElement, 0, 5).Perform();
            Thread.Sleep(5000);
            actions.Release(destinationElement).Perform();

        }
        #endregion

        #region Hover element
        public void HoverElement(IWebElement element)
        {
            _waitHelper.WaitForElementVisible(element);
            Actions actions = new Actions(_driver);
            //actions.DragAndDrop(draggedElement, destinationElement)
            //       .Perform();
            actions.MoveToElement(element).Perform();
        }
        #endregion

        #region Resize element
        public void ResizeElement(IWebElement resizedElement, IWebElement destinationElement)
        {
            Actions actions = new Actions(_driver);
            actions.ClickAndHold(resizedElement).MoveToElement(destinationElement).Build().Perform();
        }
        #endregion

        #region Check option is existing in selection
        public bool CheckExistingOption(IWebElement selElem, string itemText)
        {
            bool found = false;
            var sel = new SelectElement(selElem);
            for (int i = 0; i < sel.Options.Count; i++)
            {
                if (sel.Options[i].Text.Equals(itemText))
                {
                    found = true;
                    break;
                }
            }

            return found;
        }
        #endregion

        #region Check Element Exist
        public bool IsExist(By by)
        {
            if (_driver.FindElements(by).Count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Take screenshot
        public Screenshot SaveScreenshot()
        {
            var takeScreenshot = (ITakesScreenshot)_driver;
            return takeScreenshot.GetScreenshot();

            //return _driver.TakeScreenshot(new VerticalCombineDecorator(new ScreenshotMaker()));
        }
        #endregion

        #region Select date in datetime picker
        public void SelectTheNextDay()
        {
            Actions actions = new Actions(_driver);
            actions.SendKeys(Keys.ArrowRight).SendKeys(Keys.Escape).Perform();
        }
        #endregion
    }
}
