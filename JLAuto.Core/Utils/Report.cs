﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using AngleSharp.Common;
using JLAuto.Factory;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace JLAuto.Utils
{
    public class ReportUtil
    {
    }

    public interface ITestUtil
    {
        ExtentTest _test { get; }
        TestContext TestContext { get; set; }
    }

    public class TestUtil : ITestUtil
    {
        public ExtentTest _test { get; }
        public TestContext TestContext { get; set; }

        #region Constructor
        private TestUtil()
        {

        }
        private TestUtil(ExtentTest test)
        {
            _test = test;
        }

        public static TestUtil Instance
        {
            get
            {
                return new Lazy<TestUtil>(new TestUtil()).Value;
            }
        }
        public static TestUtil Using(ExtentTest test) => new Lazy<TestUtil>(new TestUtil(test)).Value;
        #endregion

        public void LogInfo(string description)
            => _test.Info(description);

        public void LogPass(string description)
            => _test.Pass(description);

        public void LogPass(DriverFactory driverFactory, string desc)
        {
            _test.Pass(desc);
            SaveScreenshot(driverFactory);
        }
        /// <summary>
        /// Write log with Base64 screenshot
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="ex"></param>
        public void LogFail(DriverFactory driverFactory, Exception ex)
        {
            var innerEx = ex.InnerException;
            string message = innerEx != null ? innerEx.Message : ex.Message;
            _test.Fail($"<b>{message}</b><br />{ex.StackTrace}");

            SaveScreenshot(driverFactory);
        }
        public void LogFail(string message, Exception ex)
            => _test.Fail($"<b>{message}</b><br />{ex.Message}<br />{ex.StackTrace}");
        public void LogFail(string message)
            => _test.Fail(message);
        public void LogFail(Exception ex)
            => _test.Fail($"<b>{ex.Message}</b><br />{ex.StackTrace}");


        public void SaveScreenshot(DriverFactory driverFactory)
        {
            //var screenshot = new Keywords(driverFactory)
            //                         .SaveScreenshot();

            ////_test.AddScreenCaptureFromBase64String(Convert.ToBase64String(screenshot));
            //_test.AddScreenCaptureFromBase64String(screenshot.AsBase64EncodedString);
            //var folder = Path.Combine(BasePath.DefaultPath.FullName, "TestEvidence");

            //if (!Directory.Exists(folder))
            //{
            //    Directory.CreateDirectory(folder);
            //}
            //var fileName = Path.Combine(folder, TestContext.TestName + $"_{Guid.NewGuid()}.png");

            //Image image;
            //using (MemoryStream ms = new MemoryStream(screenshot.AsByteArray))
            //{
            //    image = Image.FromStream(ms);
            //}

            //image.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);
            //TestContext.AddResultFile(fileName);

        }

        //public void SaveScreenshot(MobileAppFactory mobileAppFactory)
        //{
        //    var screenshot = new Keywords(mobileAppFactory)
        //    .SaveScreenshot();

        //    _test.AddScreenCaptureFromBase64String(screenshot.AsBase64EncodedString);
        //    var folder = Path.Combine(BasePath.DefaultPath.FullName, "TestEvidence");

        //    if (!Directory.Exists(folder))
        //    {
        //        Directory.CreateDirectory(folder);
        //    }
        //    var fileName = Path.Combine(folder, TestContext.TestName + $"_{Guid.NewGuid()}.png");

        //    System.Drawing.Image image;
        //    using (MemoryStream ms = new MemoryStream(screenshot.AsByteArray))
        //    {
        //        image = Image.FromStream(ms);
        //    }

        //    image.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);
        //    TestContext.AddResultFile(fileName);

        //}

        //public void LogPass(MobileAppFactory mobileAppFactory, string desc)
        //{
        //    _test.Pass(desc);
        //    SaveScreenshot(mobileAppFactory);
        //}

        //public void LogFail(MobileAppFactory mobileAppFactory, Exception ex)
        //{
        //    var innerEx = ex.InnerException;
        //    string message = innerEx != null ? innerEx.Message : ex.Message;
        //    _test.Fail($"<b>{message}</b><br />{ex.StackTrace}");

        //    SaveScreenshot(mobileAppFactory);
        //}
    }
}
