﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace JLAuto.Utils
{
    [AttributeUsageAttribute(AttributeTargets.All)]
    public class ResolutionAttribute : DescriptionAttribute
    {
        public ResolutionAttribute(string resolution)
        {
            Resolution = resolution;
        }

        public string Resolution { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class PlatformAttribute : DescriptionAttribute
    {
        public PlatformAttribute(string platform)
        {
            Platform = platform;
        }

        public string Platform { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class DeviceNameAttribute : DescriptionAttribute
    {
        public DeviceNameAttribute(string deviceName)
        {
            DeviceName = deviceName;
        }
        public string DeviceName { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class AutomationNameAttribute : DescriptionAttribute
    {
        public AutomationNameAttribute(string automationName)
        {
            AutomationName = automationName;
        }
        public string AutomationName { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class AppAttribute : DescriptionAttribute
    {
        public AppAttribute(string app)
        {
            App = app;
        }
        public string App { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class IsLiteAttribute : DescriptionAttribute
    {
        public IsLiteAttribute(bool mode)
        {
            Mode = mode;
        }
        public bool Mode { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class UuidAttribute : DescriptionAttribute
    {
        public UuidAttribute(string uuid)
        {
            UUID = uuid;
        }
        public string UUID { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class IdAttribute : DescriptionAttribute
    {
        public IdAttribute(string id)
        {
            Id = id;
        }
        public string Id { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class PlatformVersionAttribute : DescriptionAttribute
    {
        public PlatformVersionAttribute(string platformVersion)
        {
            PlatformVersion = platformVersion;
        }

        public string PlatformVersion { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class IsRealDeviceAttribute : DescriptionAttribute
    {
        public IsRealDeviceAttribute(bool isRealDevice)
        {
            IsRealDevice = isRealDevice;
        }

        public bool IsRealDevice { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class IsCloudDeviceAttribute : DescriptionAttribute
    {
        public IsCloudDeviceAttribute(bool isCloudDevice)
        {
            IsCloudDevice = isCloudDevice;
        }

        public bool IsCloudDevice { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class AppiumPortAttribute : DescriptionAttribute
    {
        public AppiumPortAttribute(int appiumPort)
        {
            AppiumPort = appiumPort;
        }

        public int AppiumPort { get; private set; }
    }

    [AttributeUsageAttribute(AttributeTargets.All)]
    public class WdaPortAttribute : DescriptionAttribute
    {
        public WdaPortAttribute(int wdaPort)
        {
            WdaPort = wdaPort;
        }

        public int WdaPort { get; private set; }
    }

    public static class EnumHelper
    {
        public static string GetResolution(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);
            if (name == null) { return null; }

            var field = type.GetField(name);
            if (field == null) { return null; }

            var attr = Attribute.GetCustomAttribute(field, typeof(ResolutionAttribute)) as ResolutionAttribute;
            if (attr == null) { return null; }

            return attr.Resolution;
        }
        public static string GetPlatform(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);
            if (name == null) { return null; }

            var field = type.GetField(name);
            if (field == null) { return null; }

            var attr = Attribute.GetCustomAttribute(field, typeof(PlatformAttribute)) as PlatformAttribute;
            if (attr == null) { return null; }

            return attr.Platform;
        }
        public static string GetDeviceName(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);
            if (name == null) { return null; }

            var field = type.GetField(name);
            if (field == null) { return null; }

            var attr = Attribute.GetCustomAttribute(field, typeof(DeviceNameAttribute)) as DeviceNameAttribute;
            if (attr == null) { return null; }

            return attr.DeviceName;
        }
        public static string GetAutomationName(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);
            if (name == null) { return null; }

            var field = type.GetField(name);
            if (field == null) { return null; }

            var attr = Attribute.GetCustomAttribute(field, typeof(AutomationNameAttribute)) as AutomationNameAttribute;
            if (attr == null) { return null; }

            return attr.AutomationName;
        }
        public static string GetApp(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);
            if (name == null) { return null; }

            var field = type.GetField(name);
            if (field == null) { return null; }

            var attr = Attribute.GetCustomAttribute(field, typeof(AppAttribute)) as AppAttribute;
            if (attr == null) { return null; }

            return attr.App;
        }
        public static string GetDescription(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);
            if (name == null) { return null; }

            var field = type.GetField(name);
            if (field == null) { return null; }

            var attr = Attribute.GetCustomAttributes(field, typeof(DescriptionAttribute)).FirstOrDefault() as DescriptionAttribute;
            if (attr == null) { return null; }

            return attr.Description;
        }
        public static string GetId(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);
            if (name == null) { return null; }

            var field = type.GetField(name);
            if (field == null) { return null; }

            var attr = Attribute.GetCustomAttributes(field, typeof(IdAttribute)).FirstOrDefault() as IdAttribute;
            if (attr == null) { return null; }

            return attr.Id;
        }
        public static string GetUuid(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);
            if (name == null) { return null; }

            var field = type.GetField(name);
            if (field == null) { return null; }

            var attr = Attribute.GetCustomAttribute(field, typeof(UuidAttribute)) as UuidAttribute;
            if (attr == null) { return null; }

            return attr.UUID;
        }
        public static bool GetIsLite(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);

            var field = type.GetField(name);

            var attr = Attribute.GetCustomAttribute(field, typeof(IsLiteAttribute)) as IsLiteAttribute;

            return attr.Mode;
        }
        public static string GetPlatformVersion(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);

            var field = type.GetField(name);

            var attr = Attribute.GetCustomAttribute(field, typeof(PlatformVersionAttribute)) as PlatformVersionAttribute;

            return attr.PlatformVersion;
        }
        public static bool GetIsRealDevice(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);

            var field = type.GetField(name);

            var attr = Attribute.GetCustomAttribute(field, typeof(IsRealDeviceAttribute)) as IsRealDeviceAttribute;

            return attr.IsRealDevice;
        }
        public static bool GetIsCloudDevice(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);

            var field = type.GetField(name);

            var attr = Attribute.GetCustomAttribute(field, typeof(IsCloudDeviceAttribute)) as IsCloudDeviceAttribute;

            return attr.IsCloudDevice;
        }
        public static int GetAppiumPort(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);

            var field = type.GetField(name);
            var attr = Attribute.GetCustomAttribute(field, typeof(AppiumPortAttribute)) as AppiumPortAttribute;

            return attr.AppiumPort;
        }
        public static int GetWdaPort(this Enum value)
        {
            var type = value.GetType();

            string name = Enum.GetName(type, value);

            var field = type.GetField(name);
            var attr = Attribute.GetCustomAttribute(field, typeof(WdaPortAttribute)) as WdaPortAttribute;

            return attr.WdaPort;
        }

        public static T GetEnumByDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum)
            {
                throw new ArgumentException("This cannot be a enum");
            }

            FieldInfo[] fields = type.GetFields();
            var field = fields
                            .SelectMany(f => f.GetCustomAttributes(
                                typeof(DescriptionAttribute), false), (
                                    f, a) => new { Field = f, Att = a })
                            .SingleOrDefault(a => ((DescriptionAttribute)a.Att)
                                .Description == description);
            return field == null ? default(T) : (T)field.Field.GetRawConstantValue();
        }

        public static T GetEnumByValue<T>(string value, bool ignoreCase = false) where T : new()
            => (T)Enum.Parse(typeof(T), value, ignoreCase);
    }
}
