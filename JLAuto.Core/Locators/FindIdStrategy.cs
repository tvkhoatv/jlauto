﻿using OpenQA.Selenium;

namespace JLAuto.Locators
{
    public class FindIdStrategy
    {
        IWebDriver WrappedDriver;

        public FindIdStrategy(IWebDriver wrappedDriver) //: base(value)
        {
            WrappedDriver = wrappedDriver;
        }

        public OpenQA.Selenium.IWebElement Find(string Id)
        {
            return WrappedDriver.FindElement(OpenQA.Selenium.By.Id(Id));
        }
    }
}
