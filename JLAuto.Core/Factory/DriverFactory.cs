﻿using JLAuto.Enums;
using JLAuto.Settings;
using JLAuto.Utils;
using Microsoft.Extensions.Options;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.Json;
using System.Threading;

namespace JLAuto.Factory
{
    public sealed class DriverFactory
    {
        private static DriverOptions? options;
        public static IDictionary<string, IWebDriver> Dict = new Dictionary<string, IWebDriver>();
        public string InstanceTestEvidenceDir;
        public static IWebDriver? Driver { get; set; }
        public IWebDriver? PDriver { get; set; }
        public string BrowserType { get; set; }
        public int ThreadNo { get; set; } //for report

        public DriverFactory() { }

        public DriverFactory(DriverOptions _options)
        {
            options = _options;
        }

        public DriverFactory(IOptions<DriverOptions> _options)
        {
            options = _options.Value;
        }

        private IWebDriver Initialize<T>(T browserType) where T : Enum
        {
            IWebDriver webDriverdriver = null;
            switch (browserType.ToString())
            {
                case "Firefox":
                    webDriverdriver = new FirefoxBrowser(new SessionSettings()).Init();
                    break;
                case "Chrome":
                    webDriverdriver = new ChromeBrowser(new SessionSettings()).Init();
                    break;
                case "Edge":
                    webDriverdriver = new EdgeBrowser(new SessionSettings()).Init();
                    break;
                default:
                    throw new Exception($"No factory registered for {browserType} browser.");
            }

            if (webDriverdriver == null)
            {
                throw new Exception($"No factory registered for {browserType} browser.");
            }

            BrowserType = browserType.ToString();
            PDriver = webDriverdriver;
            InstanceTestEvidenceDir = options.InstanceTestEvidenceDir;

            PDriver.Manage().Timeouts().PageLoad = new TimeSpan(0, 2, 30);

            lock (Dict)
            {
                string ThreadId = Thread.CurrentThread?.ManagedThreadId != null ? Thread.CurrentThread.ManagedThreadId.ToString() : Guid.NewGuid().ToString();
                ThreadNo = Thread.CurrentThread.ManagedThreadId;
                Dict.TryAdd(ThreadId, webDriverdriver);
            }

            return PDriver;
        }

        public DriverFactory InitDriver<T>(T browserType) where T : Enum
        {
            this.Initialize<T>(browserType);
            return this;
        }

        #region Docker detect local file in container
        public void AllowsFileDetection()
        {
            if (PDriver is IAllowsFileDetection allowsDetection)
            {
                allowsDetection.FileDetector = new LocalFileDetector();
            }
        }
        #endregion

        public IWebDriver GetDriver()
        {
            var webDriverdriver = GetDriverByThreadId(Thread.CurrentThread.ManagedThreadId);

            if (webDriverdriver != null)
            {
                PDriver = webDriverdriver;
                return PDriver;
            }

            return (IWebDriver)Initialize(options.BrowserType);
        }

        public void RevertDriver(IWebDriver? driver = null)
        {
            if (driver != null)
            {
                Dict.TryAdd(Thread.CurrentThread.ManagedThreadId.ToString(), (IWebDriver)driver);
            }
            else if (PDriver != null)
            {
                Dict.TryAdd(Thread.CurrentThread.ManagedThreadId.ToString(), (IWebDriver)PDriver);
            }
            PDriver = null;
        }

        public IWebDriver? GetDriverByThreadId(int threadId)
        {
            IWebDriver webDriverdriver = null;
            if (Dict.ContainsKey(threadId.ToString()))
            {
                _ = Dict.TryGetValue(threadId.ToString(), out webDriverdriver);
            }
            return webDriverdriver;
        }

        public void CloseDriver(string threadId = null)
        {
            if (threadId == null)
                threadId = Thread.CurrentThread.ManagedThreadId.ToString();

            if (Dict.ContainsKey(threadId))
            {
                _ = Dict.TryGetValue(threadId.ToString(), out IWebDriver? PDriver);
                if (PDriver != null)
                {
                    PDriver?.Close();
                    PDriver?.Quit();
                    PDriver?.Dispose();
                }
                Dict.Remove(threadId);
            }
        }

        #region Dispose
        public void Dispose()
        {
            if (Dict.Count <= 0)
                return;

            lock (Dict)
            {
                foreach (KeyValuePair<string, IWebDriver> item in Dict)
                {
                    item.Value?.Close();
                    item.Value?.Quit();
                    item.Value?.Dispose();
                }

                Dict.Clear();
            }
        }
        #endregion

        #region Refresh page
        public void RefreshPage() => PDriver?.Navigate().Refresh();
        #endregion

        #region Get current url
        public string? GetCurrentURL() => PDriver?.Url;
        #endregion

        #region Switch tab
        public void SwitchToLastWinDows()
        {
            var windows = PDriver?.WindowHandles;
            PDriver?.SwitchTo().Window(windows.LastOrDefault());
        }

        public void SwitchToFirstWinDows()
        {
            var windows = PDriver?.WindowHandles;
            PDriver?.SwitchTo().Window(windows.First());
        }
        #endregion

        #region Close current tab
        public void CloseCurrentTab()
        {
            if (PDriver != null)
            {
                PDriver.Close();
            }
        }
        #endregion

        #region Navigate to URl
        public DriverFactory NavigateToUrl(string url, TestUtil testUtil = null)
        {
            PDriver.Navigate().GoToUrl(url);
            testUtil?.LogInfo(PDriver.Url);
            return this;
        }
        #endregion

        #region Cookies
        public string GetAllCookiesAsString() //
        {
            var cookies = PDriver?.Manage().Cookies.AllCookies;
            return System.Text.Json.JsonSerializer.Serialize(cookies, new JsonSerializerOptions { WriteIndented = true });
        }

        public void SetCookies(string json)
        {
            var cookies = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json)!;
            foreach (var c in cookies)
            {
                string name = c.Name;
                string value = c.Value;
                string domain = c.Domain;
                string path = c.Path;
                DateTime? expiry = c.Expiry;
                bool secure = c.Secure;
                bool isHttpOnly = c.IsHttpOnly;
                string sameSite = c.SameSite;

                //var cookie = new Cookie(name, value, domain, path, expiry, secure, isHttpOnly, sameSite);
                var cookie = new Cookie(name, value, path, expiry);

                PDriver?.Manage().Cookies.AddCookie(cookie);
            }
        }

        public void ClearCookies()
        {
            PDriver?.Manage().Cookies.DeleteAllCookies();
        }
        #endregion

        #region Accept alert
        public void AcceptAlert()
        {
            PDriver?.SwitchTo().Alert().Accept();
        }
        #endregion

        #region Resize windows
        public void Resize(int width, int heigh)
            => PDriver.Manage().Window.Size = new Size(width, heigh);
        #endregion
    }
}