﻿using JLAuto.Enums;

namespace JLAuto.Factory
{
    public class DriverOptions
    {
        public BrowserType BrowserType { get; set; }

        public string? InstanceTestEvidenceDir { get; set; }
    }
}
