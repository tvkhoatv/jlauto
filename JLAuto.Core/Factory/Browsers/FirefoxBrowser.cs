﻿using JLAuto.Enums;
using JLAuto.Settings;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using WebDriverManager.DriverConfigs.Impl;

namespace JLAuto.Factory
{
    public class FirefoxBrowser : IBrowserFactory
    {
        public BrowserType BrowserType { get; set; }
        private readonly SessionSettings _options;
        private readonly string[] implicitlyDownloadedFileTypes = new[] {
            "text/csv" ,"text/plain"
        };

        public FirefoxBrowser(SessionSettings options)
        {
            _options = options;
        }

        public IWebDriver Init()
        {
            new WebDriverManager.DriverManager().SetUpDriver(new FirefoxConfig(), "MatchingBrowser");
            var options = new FirefoxOptions();
            if (_options.Headless)
            {
                options.AddArgument("-headless");
            }
            options.AddArgument("-private");
            options.SetPreference("browser.download.folderList", 2);
            options.SetPreference("browser.download.dir", _options.DownloadDirectory);
            options.SetPreference("network.cookie.cookieBehavior", 0);
            options.SetPreference("browser.helperApps.neverAsk.saveToDisk", string.Join(",", implicitlyDownloadedFileTypes));

            var driver = new FirefoxDriver(options);
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().PageLoad = new System.TimeSpan(0, 2, 30);
            return driver;
        }
    }
}
