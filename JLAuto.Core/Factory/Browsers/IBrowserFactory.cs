﻿using JLAuto.Enums;
using OpenQA.Selenium;

namespace JLAuto.Factory
{
    public interface IBrowserFactory
    {
        BrowserType BrowserType { get; set; }

        public IWebDriver Init();
    }
}
