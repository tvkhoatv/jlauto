﻿using JLAuto.Enums;
using JLAuto.Settings;
using OpenQA.Selenium;
using OpenQA.Selenium.Edge;
using WebDriverManager.DriverConfigs.Impl;

namespace JLAuto.Factory
{
    public class EdgeBrowser : IBrowserFactory
    {
        public BrowserType BrowserType { get; set; }

        private readonly SessionSettings _options;
        public EdgeBrowser(SessionSettings options)
        {
            _options = options;
        }

        public IWebDriver Init()
        {
            new WebDriverManager.DriverManager().SetUpDriver(new EdgeConfig(), "MatchingBrowser");
            EdgeOptions options = new EdgeOptions();
            options.PageLoadStrategy = PageLoadStrategy.Normal;
            options.AddArguments("--log-level=3");
            options.AddArguments("--ignore-certificate-errors");
            options.AddArguments("--ignore-ssl-errors");
            options.AddArguments("--no-sandbox");
            options.AddArguments("--disable-dev-shm-usage");
            options.UseWebView = false;

            return new EdgeDriver(options);
        }
    }
}
