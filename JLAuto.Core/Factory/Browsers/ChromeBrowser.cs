﻿using JLAuto.Enums;
using JLAuto.Settings;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using WebDriverManager.DriverConfigs.Impl;

namespace JLAuto.Factory
{
    public class ChromeBrowser : IBrowserFactory
    {
        public BrowserType BrowserType { get; set; }

        private readonly SessionSettings _options;

        public ChromeBrowser(SessionSettings options)
        {
            _options = options;
        }

        public IWebDriver Init()
        {
            new WebDriverManager.DriverManager().SetUpDriver(new ChromeConfig());
            ChromeOptions options = new ChromeOptions();
            if (_options.Headless)
            {
                options.AddArgument("headless");
            }
            options.AddArgument("--no-sandbox");
            options.AddArgument("--start-maximized");
            options.AddUserProfilePreference("download.default_directory", _options.DownloadDirectory);
            options.AddUserProfilePreference("profile.cookie_controls_mode", 0);
            options.SetLoggingPreference(LogType.Browser, LogLevel.All);

            return new ChromeDriver(options);
        }
    }
}
