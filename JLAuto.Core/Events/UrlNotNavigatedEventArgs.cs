﻿using System;

namespace JLAuto.Events
{
    public class UrlNotNavigatedEventArgs
    {
        public UrlNotNavigatedEventArgs(Exception exp) => Exception = exp;

        public Exception Exception { get; }
    }
}
