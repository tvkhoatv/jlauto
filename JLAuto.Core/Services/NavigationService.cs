﻿using OpenQA.Selenium.Support.UI;
using System.Collections.Specialized;
using System.Web;
using JLAuto.Events;
using JLAuto.Factory;
using JLAuto.Settings;
using System;

namespace JLAuto.Services
{
    public class NavigationService : WebService
    {
        public NavigationService(DriverFactory driverFactory) : base(driverFactory)
        {
        }

        public static event EventHandler<UrlNotNavigatedEventArgs>? UrlNotNavigatedEvent;

        public static event EventHandler<UrlNavigatedEventArgs>? UrlNavigatedEvent;

        public void Navigate(Uri uri)
        {
            WrappedDriver.Navigate().GoToUrl(uri);
            UrlNavigatedEvent?.Invoke(this, new UrlNavigatedEventArgs(uri.ToString()));
        }

        public void Navigate(string url)
        {
            bool tryAgain = false;
            try
            {
                WrappedDriver.Navigate().GoToUrl(url);
                UrlNavigatedEvent?.Invoke(this, new UrlNavigatedEventArgs(url));
            }
            catch (Exception)
            {
                tryAgain = true;
            }

            if (tryAgain)
            {
                try
                {
                    WrappedDriver.Navigate().GoToUrl(url);
                    UrlNavigatedEvent?.Invoke(this, new UrlNavigatedEventArgs(url));
                }
                catch (Exception ex)
                {
                    UrlNotNavigatedEvent?.Invoke(this, new UrlNotNavigatedEventArgs(ex));
                    throw new Exception($"Navigation to page {url} has failed after two attempts. Error was: {ex.Message}");
                }
            }
        }

        public void WaitForPartialUrl(string partialUrl)
        {
            try
            {
                var wait = new WebDriverWait(WrappedDriver, TimeSpan.FromSeconds(ConfigurationService.GetSection<WebSettings>().TimeoutSettings.WaitForPartialUrl));
                wait.Until((d) => WrappedDriver.Url.ToLower().Contains(partialUrl.ToLower()));
            }
            catch (Exception ex)
            {
                UrlNotNavigatedEvent?.Invoke(this, new UrlNotNavigatedEventArgs(ex));
                throw;
            }
        }

        public string GetQueryParameter(string parameterName)
        {
            var currentBrowserUrl = WrappedDriver.Url;
            Uri uri = new Uri(currentBrowserUrl);

            return HttpUtility.ParseQueryString(uri.Query).Get(parameterName);
        }

        public string SetQueryParameter(string url, string parameterName, string parameterValue)
        {
            Uri uri = new Uri(url);
            NameValueCollection query = HttpUtility.ParseQueryString(uri.Query);
            query.Add(new NameValueCollection() { { parameterName, parameterValue } });
            return uri.GetLeftPart(UriPartial.Path) + "?" + query.ToString();
        }
    }
}
