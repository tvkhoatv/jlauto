﻿using JLAuto.Utils;
using JLAuto.Settings;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Linq;

namespace JLAuto
{
    public sealed class ConfigurationService
    {
        private static IConfigurationRoot _root;

        static ConfigurationService()
        {
            _root = InitializeConfiguration();
        }

        public static TSection GetSection<TSection>() where TSection : class, new()
        {
            string sectionName = MakeFirstLetterToUpper(typeof(TSection).Name);
            return _root.GetSection(sectionName).Get<TSection>();
        }

        public static dynamic GetSection(string sectionName)
        {
            return _root.GetSection(sectionName).GetChildren();
        }

        private static string MakeFirstLetterToUpper(string text)
        {
            return char.ToUpper(text[0]) + text.Substring(1);
        }

        private static IConfigurationRoot InitializeConfiguration()
        {
            var projectDir = ExecutionDirectoryResolver.GetRootPath();
            var environment = RuntimeSetting.Instance.GetProperty("Environment");
            var filesInExecutionDir = Directory.GetFiles(projectDir);
            var builder = new ConfigurationBuilder().SetBasePath(projectDir);
            var settingsFile =
                filesInExecutionDir.FirstOrDefault(x => x.Contains("environment") && x.Contains(environment != "" ? "." + environment.ToString() : "") && x.EndsWith(".json"));

            if (settingsFile != null)
            {
                builder.AddJsonFile(settingsFile, optional: true, reloadOnChange: true);
            }

            builder.AddEnvironmentVariables();

            return builder.Build();
        }

        public static void InitializeConfiguationBinding<T>(ref T model) where T: class
        {
            _root.Bind(model);
        }
    }
}
