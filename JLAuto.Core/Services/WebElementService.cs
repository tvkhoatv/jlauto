﻿using JLAuto.Factory;
using JLAuto.Locators;
using OpenQA.Selenium;

namespace JLAuto.Services
{
    public class WebElementService : WebService
    {
        public WebElementService(DriverFactory driverFactory) : base(driverFactory)
        {
        }

        public IWebElement FindById(string id, bool shouldCacheElement = false)
            => (new FindIdStrategy(WrappedDriver).Find(id));

    }
}
