﻿using JLAuto.Factory;
using OpenQA.Selenium;

namespace JLAuto
{
    public abstract class WebService
    {
        public IWebDriver WrappedDriver;

        protected WebService(DriverFactory driverFactory)
        {
            WrappedDriver = driverFactory.GetDriver();
        }

        protected WebService(IWebDriver wrappedDriver)
        {
            WrappedDriver = wrappedDriver;
        }
    }
}
