﻿using System.Text;
using System.Text.RegularExpressions;

namespace JLAuto
{
    public static class StringExtensions
    {
        public static string ExtractPattern(this string source, string regex) => new Regex(regex).Match(source).Value;

        #region Split string by upercase
        public static string SplitByUperCase(this string stringToSplit)
        {
            if (!string.IsNullOrEmpty(stringToSplit))
            {
                StringBuilder builder = new StringBuilder();
                foreach (char c in stringToSplit)
                {
                    if (System.Char.IsUpper(c))
                        builder.Append(' ');
                    builder.Append(c);
                }
                return builder.ToString();
            }
            else
                return null;
        }
        #endregion

        public static string SubStringGetLast(this string needToSubString, char character = '.')
        {
            if (!string.IsNullOrEmpty(needToSubString))
            {
                int index = needToSubString.LastIndexOf(character);
                if (index > 0)
                {
                    return needToSubString.Substring(index + 1);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static string SubStringGetFirst(this string needToSubString, char character = '.')
        {
            if (!string.IsNullOrEmpty(needToSubString) && needToSubString.Contains(character))
            {
                int index = needToSubString.LastIndexOf(character);
                if (index > 0)
                {
                    return needToSubString.Substring(0, index);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return needToSubString;
            }
        }
    }
}
