﻿using System.Linq;

namespace JLAuto
{
    public static class ReflectionExtensions
    {
        public static string GetFriendlyTypeName(this object target)
        {
            var type = target as System.Type ?? target.GetType();
            var name = type.Name;
            return type.IsGenericType ? $"{name.ExtractPattern("\\w+")}<{string.Join(",", type.GetGenericArguments().Select(t => t.GetFriendlyTypeName()))}>" : name;
        }
    }
}