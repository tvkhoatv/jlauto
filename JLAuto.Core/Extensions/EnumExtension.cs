﻿using System;

namespace JLAuto
{
    public static class EnumExtension
    {
        public static String convertToString(this Enum eff)
        {
            return Enum.GetName(eff.GetType(), eff);
        }
    }
}
