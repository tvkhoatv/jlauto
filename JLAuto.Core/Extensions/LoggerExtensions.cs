﻿using Microsoft.VisualStudio.TestTools.UnitTesting.Logging;
using Newtonsoft.Json;
using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace JLAuto
{
    public class LoggerEx : Logger
    {
        private static readonly object _lockObject = new object();

        public static void LogInformation(string message, params object[] args)
        {
            lock (_lockObject)
            {
                try
                {
                    Console.WriteLine(message, args);
                }
                catch
                {
                    // ignore
                }
            }
        }

        public static void LogWarning(string message, params object[] args)
        {
            lock (_lockObject)
            {
                try
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(message, args);
                    Console.ResetColor();
                }
                catch
                {
                    // ignore
                }
            }
        }

        public static void LogError(string message, params object[] args)
        {
            lock (_lockObject)
            {
                try
                {
                    Console.Error.WriteLine(message, args);
                }
                catch
                {
                    // ignore
                }
            }
        }

        public static void ErrorData(object obj, Exception? ex = null, string note = "", [CallerMemberName] string memberName = "")
        {
            StringBuilder builder = new StringBuilder();
            if (memberName != "")
                builder.Append($"Error in function {memberName}. ");
            if (note != "")
                builder.Append($"Note: {note}. ");
            if (ex != null)
                builder.Append($"Exception: {ex.Message}, ");

            var json = JsonConvert.SerializeObject(obj,
                    Formatting.Indented,
                    new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
            var objName = obj.GetType().Name;
            builder.Append($"Object name: {objName}, Data: {json}");
            if (ex != null)
                LogError(builder.ToString(), ex);
            else
                LogError(builder.ToString());
        }
    }
}
