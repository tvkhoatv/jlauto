﻿using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium;
using JLAuto.Factory;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;

namespace JLAuto.Infrastructure.Base
{
    public class NewExpectedConditions
    {
        private IWebDriver _driver;

        #region Constructor
        public NewExpectedConditions(IWebDriver driver)
        {
            _driver = driver;
        }
        #endregion

        #region Element Visible
        public Func<IWebDriver, bool> ElementVisible(IWebElement element)
        {
            return (Driver) => { return element != null && element.Displayed; };
        }
        #endregion

        #region Elements visible
        public Func<IWebDriver, bool> ElementsVisible(IList<IWebElement> elements)
            => (Driver) => { return elements != null && elements.Any(x => x.Displayed); };
        #endregion

        #region Element invisible
        public Func<IWebDriver, bool> ElementInvisible(IWebElement element)
        {
            return (_driver) => { return !element.Enabled; };
        }
        #endregion

        #region Element enable
        public Func<IWebDriver, bool> ElementEnabled(IWebElement element)
        {
            return (Driver) => { return element.Enabled; };
        }
        #endregion

        #region Element present
        public Func<IWebDriver, bool> ElementPresented(IWebElement element)
        {
            return (Driver) => { return element.Displayed; };
        }
        #endregion

        #region Element not present
        public Func<IWebDriver, bool> ElementNotPresented(IWebElement element)
        {
            return (Driver) => { return !element.Displayed; };
        }
        #endregion

        #region Elements not present
        public Func<IWebDriver, bool> ElementNotPresented(IList<IWebElement> elements)
            => (Driver) => { return elements != null && elements.All(x => !x.Displayed); };
        #endregion

        #region Element has attribute disable
        public Func<IWebDriver, bool> AttributeDisable(IWebElement element, string attribute)
        {
            return (Driver) => { return string.IsNullOrEmpty(element.GetAttribute(attribute)); };
        }
        #endregion

        #region Element has attribute
        public Func<IWebDriver, bool> AttributeVisible(IWebElement element, string attribute)
        {
            return (Driver) => { return !string.IsNullOrEmpty(element.GetAttribute(attribute)); };
        }
        #endregion

        #region Element has attribute equals value
        public Func<IWebDriver, bool> AttributeEqualsValue(IWebElement element, string attribute, string attributeValue)
        {
            return (Driver) => { return element.GetAttribute(attribute).Equals(attributeValue); };
        }
        public Func<IWebDriver, bool> AttributeEqualsValue(By by, string attribute, string attributeValue)
        {
            return (Driver) =>
            {
                IWebElement element = Driver.FindElement(by);
                return element.GetAttribute(attribute).Equals(attributeValue);
            };
        }
        #endregion

        #region Element has attribute not contains value
        public Func<IWebDriver, bool> AttributeNotContainsValue(IWebElement element, string attribute, string attributeValue)
        {
            return (Driver) => { return !element.GetAttribute(attribute).Contains(attributeValue); };
        }
        #endregion

        #region Element has attribute contains value
        public Func<IWebDriver, bool> AttributeContainsValue(IWebElement element, string attribute, string attributeValue)
        {
            return (Driver) => { return element.GetAttribute(attribute).Contains(attributeValue); };
        }
        #endregion

        #region Elements have attribute contains value
        public Func<IWebDriver, bool> AttributeContainsValue(IList<IWebElement> elements, string attribute, string attributeValue)
        {
            return (Driver) => { return elements.Select(e => e.GetAttribute(attribute).Contains(attributeValue)).FirstOrDefault(); };
        }
        #endregion

        #region Element has not have attribute
        public Func<IWebDriver, bool> ElementNotHaveAttribute(IWebElement element, string attribute)
        {
            return (Driver) => { return string.IsNullOrEmpty(element.GetAttribute(attribute)); };
        }
        #endregion

        #region Element has text invisible
        public Func<IWebDriver, bool> TextInvisible(IWebElement element, string text)
        {
            return (Driver) => { return !element.GetAttribute("innerText").Contains(text); };
        }
        #endregion

        #region Element has text invisible
        public Func<IWebDriver, bool> TextVisible(IWebElement element, string text)
        {
            var displayedText = element.GetAttribute("innerText");
            return (Driver) => { return displayedText.Contains(text.Trim()); };
        }
        #endregion

        #region text invisible on Mobile
        public Func<IWebDriver, bool> TextVisibleMobile(string text)
        {
            var check = 0;
            if (_driver.GetType() == typeof(AndroidDriver))
            {
                check = _driver.FindElements(By.XPath("//*[contains(@text,'" + text + "')]")).Count;
            }
            else
            {
                check = _driver.FindElements(By.XPath("//*[contains(@name,'" + text + "')]")).Count;
            }
            return (Driver) => { return check > 0 ? true : false; };
        }
        #endregion

        #region Page is full-load
        public Func<IWebDriver, bool> PageFullLoad()
        {
            return (Driver) =>
            {
                IJavaScriptExecutor executor = (IJavaScriptExecutor)Driver;
                bool IsNotUndefined = (bool)executor.ExecuteScript("return window.jQuery != undefined");

                bool isAjaxFinished = IsNotUndefined ? (bool)executor.ExecuteScript("return jQuery.active == 0") : true;
                bool isLoaderHidden = (bool)executor.ExecuteScript("return $('.loading').is(':visible') == false");
                bool isLoaderImageHidden = (bool)executor.ExecuteScript("return $(\"[class='k-loading-image']\").is(':visible') == false");
                bool isLoaderBubblesHidden = (bool)executor.ExecuteScript("return $('.loading-bubbles').is(':visible') == false");
                bool isReady = executor.ExecuteScript("return document.readyState").ToString() == "complete";
                return isAjaxFinished && isLoaderHidden & isLoaderImageHidden & isLoaderBubblesHidden & isReady;
            };
        }
        #endregion

        #region Wait for file is downloaded
        public Func<IWebDriver, bool> FileDowloaded(string fullPath, string fileName)
        {
            return (Driver) =>
            {
                var folder = new DirectoryInfo(fullPath);
                var files = folder.GetFiles($"{fileName}");
                return (files.Count() > 0);
                //return File.Exists(fullFilePath);
            };
        }
        #endregion

        #region Shadowroot is fully loaded
        public Func<IWebDriver, bool> ShadowRootFullLoaded()
        {
            return (Driver) =>
            {
                IJavaScriptExecutor executor = (IJavaScriptExecutor)Driver;
                bool isVisible = (bool)executor.ExecuteScript($"return document.querySelector('vue-advanced-chat').shadowRoot.querySelector('#vac-circle') == null");

                return isVisible;
            };
        }
        #endregion
    }

    public class WaitHelper
    {
        #region Declare field
        private int _timeSpan = 30;
        private int _longTimeSpan = 120;
        private readonly IWebDriver _driver;
        private NewExpectedConditions expectedCondition;
        private WebDriverWait wait;
        private WebDriverWait longWait;
        #endregion

        #region Constructor
        public WaitHelper(DriverFactory driverFactory, int timeSpan = 30)
        {
            _driver = driverFactory.PDriver;
            _timeSpan = timeSpan;
            expectedCondition = new NewExpectedConditions(_driver);
            wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(_timeSpan));
            wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));

            longWait = new WebDriverWait(_driver, TimeSpan.FromSeconds(_longTimeSpan));
        }
        //public WaitHelper(MobileAppFactory appFactory, int timeSpan = 120)
        //{
        //    _driver = appFactory.Driver;
        //    _timeSpan = timeSpan;
        //    expectedCondition = new NewExpectedConditions(_driver);
        //    wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(_timeSpan));
        //    wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
        //}
        public WaitHelper(IWebDriver driver, int timeSpan = 120)
        {
            _driver = driver;
            _timeSpan = timeSpan;
            expectedCondition = new NewExpectedConditions(_driver);
            longWait = new WebDriverWait(_driver, TimeSpan.FromSeconds(_timeSpan));
            longWait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
        }
        #endregion

        #region Wait for element visible
        /// <summary>
        /// Wait for element is visible, default time out is 120 seconds.
        /// </summary>
        public IWebElement WaitForElementVisible(IWebElement element, bool useLongWait = false)
        {
            if (!useLongWait)
                wait.Until(expectedCondition.ElementVisible(element));
            else
                longWait.Until(expectedCondition.ElementVisible(element));
            return element;
        }
        #endregion

        #region Wait for element enable
        /// <summary>
        /// Wait for element is anable, default time out is 120 seconds.
        /// </summary>
        public IWebElement WaitForElementEnable(IWebElement element)
        {
            wait.Until(expectedCondition.ElementEnabled(element));
            return element;
        }
        #endregion

        #region Wait for element invisible
        /// <summary>
        /// Wait for element is invisible, default time out is 120 seconds.
        /// </summary>
        public void WaitForElementInvisible(IWebElement element)
        {
            wait.Until(expectedCondition.ElementInvisible(element));
        }
        #endregion

        #region Wait for element presented
        /// <summary>
        /// Wait for element is invisible, default time out is 120 seconds.
        /// </summary>
        public void WaitForElementPresent(IWebElement element)
        {
            wait.Until(expectedCondition.ElementPresented(element));
        }
        #endregion

        #region Wait for element not presented
        /// <summary>
        /// Wait for element is invisible, default time out is 120 seconds.
        /// </summary>
        public void WaitForElementNotPresented(IWebElement element)
        {

            wait.Until(expectedCondition.ElementNotPresented(element));
        }
        #endregion

        #region Wait for elements visible
        /// <summary>
        /// Wait for elements are visible, default time out is 120 seconds.
        /// </summary>
        public IList<IWebElement> WaitForElementsVisible(IList<IWebElement> elements, bool useLongWait = false)
        {
            if (!useLongWait)
                wait.Until(expectedCondition.ElementsVisible(elements));
            else
                longWait.Until(expectedCondition.ElementsVisible(elements));
            return elements;
        }
        #endregion

        #region Wait for elements not present
        public void WaitForElementNotPresented(IList<IWebElement> elements)
        {
            wait.Until(expectedCondition.ElementNotPresented(elements));
        }
        #endregion

        #region Wait for element's attribute visible
        public IWebElement WaitForAttributeVisisble(IWebElement element, string attribute)
        {
            wait.Until(expectedCondition.AttributeVisible(element, attribute));
            return element;
        }
        #endregion

        #region Wait for element's attribute invisible
        /// <summary>
        /// Wait for element has the attribute is invisible, default time out is 120 seconds.
        /// </summary>
        public IWebElement WaitForAttributeInvisible(IWebElement element, string attribute)
        {

            wait.Until(expectedCondition.AttributeDisable(element, attribute));
            return element;
        }
        #endregion

        #region Wait for element has not have attribute
        /// <summary>
        /// Wait for element does not have the attribute, default time out is 120 seconds.
        /// </summary>
        public IWebElement WaitForElementNotHaveAttribute(IWebElement element, string attribute)
        {

            wait.Until(expectedCondition.ElementNotHaveAttribute(element, attribute));
            return element;
        }
        #endregion

        #region Wait for element's atribute equals value
        /// <summary>
        /// Wait for element's attribute equals to value, default time out is 120 seconds.
        /// </summary>
        public IWebElement WaitForAttributeEqualsValue(IWebElement element, string attribute, string attributeValue)
        {

            wait.Until(expectedCondition.AttributeEqualsValue(element, attribute, attributeValue));
            return element;
        }
        #endregion

        #region Wait for element's atribure equals value by locator
        /// <summary>
        /// Wait for locator of element's attribute equals to value, default time out is 120 seconds.
        /// </summary>
        public IWebElement WaitForAttributeEqualsValue(By by, string attribute, string attributeValue)
        {

            wait.Until(expectedCondition.AttributeEqualsValue(by, attribute, attributeValue));
            return _driver.FindElement(by);
        }
        #endregion

        #region Wait for element's attribute does not contain value
        /// <summary>
        /// Wait for element's attribute does not contain value, default time out is 120 seconds.
        /// </summary>
        public IWebElement WaitForAttributeNotContainsValue(IWebElement element, string attribute, string attributeValue)
        {

            wait.Until(expectedCondition.AttributeNotContainsValue(element, attribute, attributeValue));
            return element;
        }
        #endregion

        #region Wait for element's attribute contains value
        /// <summary>
        /// Wait for element's attribute contains value, default time out is 120 seconds.
        /// </summary>
        public IWebElement WaitForAttributeContainsValue(IWebElement element, string attribute, string attributeValue)
        {

            wait.Until(expectedCondition.AttributeContainsValue(element, attribute, attributeValue));
            return element;
        }
        #endregion

        #region Wait for element's text invisible
        /// <summary>
        /// Wait for element's text is invisible, default time out is 120 seconds.
        /// </summary>
        public IWebElement WaitForTextInvisible(IWebElement element, string text)
        {

            wait.Until(expectedCondition.TextInvisible(element, text));
            return element;
        }
        #endregion

        #region Wait for element's text visible
        /// <summary>
        /// Wait for element's text is visible, default time out is 120 seconds.
        /// </summary>
        public IWebElement WaitForTextVisible(IWebElement element, string text)
        {

            wait.Until(expectedCondition.TextVisible(element, text));
            return element;
        }
        #endregion

        #region Wait for page full loaded
        /// <summary>
        /// Wait for the page is full-loaded, default time out is 120 seconds.
        /// </summary>
        public void WaitForPageFullLoaded()
        {
            int retry = 4;
            while (retry > 0)
            {
                try
                {
                    longWait.Until(expectedCondition.PageFullLoad());
                    break;
                }
                catch (WebDriverException ex)
                {
                    retry--;
                    if (retry == 0)
                        throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion

        #region Wait for shadow root fully loaded
        /// <summary>
        /// Wait for the page is full-loaded, default time out is 120 seconds.
        /// </summary>
        public void WaitForShadowRootFullLoaded()
        {
            int retry = 4;
            while (retry > 0)
            {
                try
                {
                    longWait.Until(expectedCondition.ShadowRootFullLoaded());
                    break;
                }
                catch (WebDriverException ex)
                {
                    retry--;
                    if (retry == 0)
                        throw ex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion

        #region Wait for file is downloaded
        public void WaitForFileDownloaded(string fullPath, string fileName)
        {
            try
            {
                longWait.Until(expectedCondition.FileDowloaded(fullPath, fileName));
            }
            catch (WebDriverTimeoutException ex)
            {
                string message = "File Not Found: " + Path.Combine(fullPath, fileName);
                throw new ArgumentException(message, ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Element To Be Clickable
        public IWebElement WaitForElementClickable(IWebElement element)
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
            return element;
        }
        #endregion

        #region Element No Longer attached to the DOM
        public void WaitForElementNotLocated(By element)
        {
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));
        }
        #endregion

        #region Wait for element display fully on screen
        public bool WaitForElementOnScreen(IWebElement element)
        {
            int retry = 3;
            bool isOnViewPort;

            do
            {
                retry--;
                isOnViewPort = (bool)((IJavaScriptExecutor)_driver).ExecuteScript("var elem = arguments[0], " +
                    "box = elem.getBoundingClientRect(), " +
                    "cx = box.left + box.width / 2,  " +
                    "cy = box.top + box.height / 2,  " +
                    "e = document.elementFromPoint(cx, cy); " +
                    "for (; e; e = e.parentElement) " +
                    "{ " +
                        "if (e === elem) return true;" +
                    "} return false;", element);

                if (isOnViewPort)
                    return true;
                else
                    Thread.Sleep(5000);
            } while (!isOnViewPort && retry > 0);

            return false;
        }
        #endregion

        #region Wait For Page Mobile Full Loaded
        public Boolean WaitForPageMobileFullLoaded()
        {
            while (true)
            {
                if (_driver.GetType() == typeof(AndroidDriver))
                {
                    if (_driver.FindElements(By.XPath("//*[contains(@resource-id,'progress_bar')]")).Count() == 0 &&
                        _driver.FindElements(By.XPath("//*[contains(@resource-id,'progressBar')]")).Count() == 0 &&
                        _driver.FindElements(By.XPath("//*[contains(@class,'ProgressBar')]")).Count() == 0
                        )
                    {
                        return true;
                    }
                }
                else
                {
                    if (_driver.FindElements(By.XPath("//XCUIElementTypeActivityIndicator[@name='In progress'][@visible='true']")).Count() == 0 &&
                        _driver.FindElements(By.XPath("//XCUIElementTypeStaticText[@name='Please Wait...'][@visible='true']")).Count() == 0)
                    {
                        return true;
                    }
                }

            }
        }
        #endregion

        #region Wait for element mobile displayed
        public bool WaitElementMobileDisplayed(IWebElement element, double timeout = 30)
        {
        WaitElementMobileDisplayed:
            try
            {
                wait.Timeout = TimeSpan.FromSeconds(timeout);
                wait.Until(expectedCondition.ElementVisible(element));
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
            catch (NoSuchElementException)
            {
                goto WaitElementMobileDisplayed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                wait.Timeout = TimeSpan.FromSeconds(_timeSpan);
            }
        }
        #endregion

        #region Wait for element mobile not display
        public bool WaitElementMobileNotDisplayed(IWebElement element, double timeout = 30)
        {
            try
            {
                wait.Timeout = TimeSpan.FromSeconds(timeout);
                wait.Until(expectedCondition.ElementNotPresented(element));
                return false;
            }
            catch (WebDriverTimeoutException ex)
            {
                return true;
            }
            catch (Exception ex)
            {
                throw ex; ;
            }
            finally
            {
                wait.Timeout = TimeSpan.FromSeconds(_timeSpan);
            }
        }
        #endregion

        #region Wait for mobile notification displayed
        public bool WaitMobileFloatingNotificationDisplay(IWebElement element, string message, double timeout = 60)
        {
            var check = false;

        WaitNotification:
            try
            {
                wait.Timeout = TimeSpan.FromSeconds(timeout);

                if (_driver.GetType() == typeof(AndroidDriver))
                    check = wait.Until(expectedCondition.AttributeContainsValue(element, "text", message));
                else
                    check = wait.Until(expectedCondition.ElementVisible(element));
            }
            catch (WebDriverTimeoutException)
            {
                check = false;
            }
            catch (NoSuchElementException)
            {
                goto WaitNotification;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                wait.Timeout = TimeSpan.FromSeconds(_timeSpan);
            }
            return check;
        }

        public bool WaitMobileNotificationDisplayInList(IWebElement element, string message, double timeout = 60)
        {
            var check = false;
            ITouchAction actions = new TouchAction((IPerformsTouchActions)_driver);
            var size = _driver.Manage().Window.Size;
            int startX = size.Width / 2;
            int startY = 10;
            int endY = size.Height;

        WaitNotification:
            try
            {
                wait.Timeout = TimeSpan.FromSeconds(timeout);
                if (_driver.GetType() == typeof(AndroidDriver))
                {
                    ((AndroidDriver)_driver).OpenNotifications();
                    check = wait.Until(expectedCondition.AttributeContainsValue(element, "text", message));
                }
                else
                {
                    actions.Press(startX, startY).Wait(1000).MoveTo(startX, endY).Release().Perform();
                    check = wait.Until(expectedCondition.AttributeContainsValue(element, "name", message));
                }
            }
            catch (WebDriverTimeoutException)
            {
                check = false;
            }
            catch (NoSuchElementException)
            {
                goto WaitNotification;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                wait.Timeout = TimeSpan.FromSeconds(_timeSpan);
            }
            return check;
        }
        #endregion

        #region Wait for mobile app active
        public void WaitAppActive()
        {
            try
            {
                if (_driver.GetType() == typeof(AndroidDriver))
                {
                    wait.Until(expectedCondition.ElementsVisible(_driver.FindElements(By.XPath("//*[contains(@resource-id, 'com.tracer.joblogic')]"))));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Wait for text mobile displayed
        public bool WaitTextMobileDisplayed(string text, double timeout = 30)
        {
            try
            {
                wait.Timeout = TimeSpan.FromSeconds(timeout);
                wait.Until(expectedCondition.TextVisibleMobile(text));
                return true;
            }
            catch (WebDriverTimeoutException ex)
            {
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                wait.Timeout = TimeSpan.FromSeconds(_timeSpan);
            }
        }
        #endregion
    }
}
