﻿using JLAuto.Infrastructure.Models;
using JLAuto.Utils;
using System.Collections.Generic;
using BrowserType = JLAuto.Enums.BrowserType;

namespace JLAuto.Infrastructure.Variables
{
    public sealed class AppConfigFactory
    {
        public static string? Environment { get; set; }
        public static string? Region { get; set; }
        public static string? Project { get; set; }
        public static Device? Device { get; set; }
        public static IEnumerable<string>? MobileDevices { get; set; }
        public static IEnumerable<BrowserType>? BrowserTypes { get; set; }
        public static string? HubUrl { get; set; }

        public static string? TestCategory { get; set; }
        public static string? TenantType { get; set; }

        public static string? Url { get; set; }

        private static IEnvironment _instance;

        public static string? BasePath { get; set; }

        static AppConfigFactory()
        {
            BasePath = ExecutionDirectoryResolver.GetRootPath();

            _instance = new Infrastructure.Models.Environment();

            ConfigurationService.InitializeConfiguationBinding(ref _instance);
        }

        public static IEnvironment Instance
        {
            get
            {
                return _instance;
            }
        }
    }
}
