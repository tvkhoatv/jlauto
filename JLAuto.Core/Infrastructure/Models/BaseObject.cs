﻿using JLAuto.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Enums;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace JLAuto.Infrastructure.Models
{
    public class Browser
    {
        public string BrowserName { get; set; }
        public bool IsCheck { get; set; }
        public Browser() { }
    }

    public class BaseObject
    {
        public Environment[] Environments { get; set; }

        public Environment SelectedEnvironment { get; set; }

        public BaseObject() { }

        public BaseObject(Environment[] environments)
        {
            Environments = environments;
        }
    }

    //public enum BrowserType
    //{
    //    Chrome,
    //    Firefox,
    //    Edge,
    //    EdgeLegacy,
    //    Safari
    //}

    public enum TenantType
    {
        None = 0,
        WebInfo = 1,
        SearchTenantInfo = 2,
        Linaker = 3,
    }

    public enum CookieType
    {
        Web,
        Portal,
        Mobile,
        SubWeb
    }
    public class HttpRequestHeaderModel
    {
        public string Authorization { get; set; }
        public string RequestVerificationToken { get; set; }
        private string region;
        public string Region 
        {
            get
            {
                region = Cookies.FirstOrDefault( x => x.Name.Contains("JLWebRegionCookie"))?.Value;
                return !string.IsNullOrEmpty(region) ? $"{region}" : string.Empty;
            }
        }
        public IReadOnlyCollection<Cookie> Cookies;
        public string CookiesString
        {
            get
            {
                string allCookies = null;
                foreach (Cookie cookie in Cookies)
                {
                    allCookies += string.Format("{0};", cookie.ToString());
                }
                return allCookies;
            }
        }
        public HttpRequestHeaderModel(IReadOnlyCollection<Cookie> cookies, string requestVerificationToke)
        {
            Cookies = cookies;
            RequestVerificationToken = requestVerificationToke;
        }
        public HttpRequestHeaderModel(string authorization)
        {
            Authorization = authorization;
        }
    }
    public class WebCookies : IReadOnlyCollection<Cookie>
    {
        #region Override interface
        public int Count => throw new System.NotImplementedException();

        public IEnumerator<Cookie> GetEnumerator()
        {
            throw new System.NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new System.NotImplementedException();
        }
        #endregion
        public override string ToString()
        {
            string allCookies = null;
            foreach (Cookie cookie in this)
            {
                allCookies += string.Format("{0};", cookie.ToString());
            }
            return allCookies;
        }
    }
    public enum MobileAppType
    {
        IOS, Android
    }

    public class DeviceName
    {
        public string DevName { get; set; }
    }

    public enum Device
    {
        [Description("Windows 10 (64-bit)")]
        [Resolution("1920x1080")]
        Windows10,

        [Description("macOS Mojave")]
        macOSMojave,

        [Description("Samsung Galaxy S9")]
        [Platform(MobilePlatform.Android)]
        [DeviceName("ce021822e3f548f80b")]
        [AutomationName("UiAutomator2")]
        [Resolution("360x640")]
        [PlatformVersion("9")]
        [IsRealDevice(true)]
        GalaxyS9,

        [Description("Galaxy Tab T295")]
        [DeviceName("R9WN50DNAPJ")]   
        [Platform(MobilePlatform.Android)]
        [PlatformVersion("11")]
        [IsRealDevice(true)]
        GalaxyTabT295,

        [Description("android_emulator_12")]
        [DeviceName("emulator-5554")]
        [Platform(MobilePlatform.Android)]
        [PlatformVersion("12")]
        [IsRealDevice(false)]
        [IsCloudDevice(false)]
        android_emulator_12,

        [Platform(MobilePlatform.Android)]
        [DeviceName("Pixel_4_API_30")]
        [AutomationName("UiAutomator2")]
        [PlatformVersion("11")]
        [IsRealDevice(false)]
        Pixel4,

        [Description("Samsung_S10")]
        [Platform(MobilePlatform.Android)]
        [DeviceName("R39M30EZ56L")]
        [AutomationName("UiAutomator2")]
        [PlatformVersion("12")]
        [IsRealDevice(true)]
        [IsCloudDevice(false)]
        Samsung_S10,

        [Description("Real_Pixel5")]
        [Platform(MobilePlatform.Android)]
        [DeviceName("0A241FDD4006CE")]
        [AutomationName("UiAutomator2")]
        [PlatformVersion("13")]
        [IsRealDevice(true)]
        [IsCloudDevice(false)]
        Real_Pixel5,

        [Platform(MobilePlatform.IOS)]
        [DeviceName("iPhone 13")]
        [AutomationName("XCUITest")]
        [PlatformVersion("15.5")]
        [Uuid("9533EFA5-AFF3-4237-858B-AEE9BD326362")]
        [IsRealDevice(false)]
        [AppiumPort(4725)]
        [IsCloudDevice(true)]
        [WdaPort(8102)]
        iPhone_Emulator_UAT,

        [Platform(MobilePlatform.IOS)]
        [DeviceName("iPhone XR Real")]
        [AutomationName("XCUITest")]
        [PlatformVersion("15.5")]
        [IsRealDevice(true)]
        [Uuid("00008020-00090CAA3EBA002E")]
        [IsCloudDevice(false)]
        iPhone_XR_Real,

        [Platform(MobilePlatform.IOS)]
        [DeviceName("iPad 9 Real")]
        [AutomationName("XCUITest")]
        [PlatformVersion("15.4.1")]
        [IsRealDevice(true)]
        [Uuid("00008030-001E452602C3C02E")]
        iPad_9_Real,

        [Platform(MobilePlatform.IOS)]
        [DeviceName("iPhone 8 Real")]
        [AutomationName("XCUITest")]
        [PlatformVersion("15.5")]
        [IsRealDevice(true)]
        [Uuid("b05a99645563c95e9c9ddf0970d3022cb21e644b")]
        [IsCloudDevice(false)]
        [AppiumPort(4724)]
        [WdaPort(8101)]
        iPhone_8_Real,

        [Description("Blue_Stack_Device")]
        [DeviceName("127.0.0.1:5555")]
        [Platform(MobilePlatform.Android)]
        [PlatformVersion("7.1")]
        [IsRealDevice(true)]
        [IsCloudDevice(false)]
        Blue_Stack_Device,

        [Platform(MobilePlatform.IOS)]
        [DeviceName("iPad 5 Real")]
        [AutomationName("XCUITest")]
        [PlatformVersion("15.4")]
        [IsRealDevice(true)]
        [Uuid("92e85b4ab8b9495caef1c9d6df45189c4acd8126")]
        [AppiumPort(4724)]
        [IsCloudDevice(false)]
        [WdaPort(8101)]
        iPad_5_Real,

        [Description("iPhone_Emulator_Live")]
        [Platform(MobilePlatform.IOS)]
        [DeviceName("iPhone 13 Pro Max")]
        [AutomationName("XCUITest")]
        [PlatformVersion("15.5")]
        [Uuid("0B0D67CD-65D5-4565-95C2-773229A9FE91")]
        [IsRealDevice(false)]
        [AppiumPort(4726)]
        [IsCloudDevice(false)]
        [WdaPort(8103)]
        iPhone_Emulator_Live,

        [Description("iPhone_Emulator_Local")]
        [Platform(MobilePlatform.IOS)]
        [DeviceName("iPhone 12")]
        [AutomationName("XCUITest")]
        [PlatformVersion("15.5")]
        [Uuid("9A2A6BA9-2E3D-4838-9415-E40366AB9CD0")]
        [IsRealDevice(false)]
        [AppiumPort(4723)]
        [IsCloudDevice(false)]
        [WdaPort(8100)]
        iPhone_Emulator_Local,

        [Description("iPhone_11_Real")]
        [Platform(MobilePlatform.IOS)]
        [DeviceName("iPhone 11")]
        [AutomationName("XCUITest")]
        [PlatformVersion("15.7.1")]
        [Uuid("00008030-0011150E0EB9802E")]
        [IsRealDevice(true)]
        [AppiumPort(4724)]
        [IsCloudDevice(true)]
        [WdaPort(8101)]
        iPhone_11_Real,

        [Description("android_emulator_12_Mac")]
        [DeviceName("emulator-5554")]
        [Platform(MobilePlatform.Android)]
        [PlatformVersion("12")]
        [IsRealDevice(false)]
        [IsCloudDevice(true)]
        android_emulator_12_Mac,

        [Description("Real_Pixel5_2")]
        [Platform(MobilePlatform.Android)]
        [DeviceName("0B191FDD4001XM")]
        [AutomationName("UiAutomator2")]
        [PlatformVersion("13")]
        [IsRealDevice(true)]
        [IsCloudDevice(false)]
        Real_Pixel5_2
    }
}
