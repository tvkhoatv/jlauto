﻿using System;
using System.Collections.Generic;

namespace JLAuto.Infrastructure.Models
{
    public interface IEnvironment
    {
        DbInfo DbInfo { get; set; }
        MongoDbInfo MongoDbInfo { get; set; }
        InitBy InitBy { get; set; }
        string Url { get; set; }
        string PortalUrl { get; set; }
        Guid TenantId { get; set; }
        AppInfo WebInfo { get; set; }
        AppInfo SearchTenantInfo { get; set; }
        MobileInfo MobileInfo { get; set; }
        PublicApiInfo PublicApiInfo { get; set; }
        JicroInfo JicroInfo { get; set; }
        TwilioInfo TwilioInfo { get; set; }
    }

    public class Environment : IEnvironment
    {
        public DbInfo DbInfo { get; set; }
        public MongoDbInfo MongoDbInfo { get; set; }
        public InitBy InitBy { get; set; }
        public Guid TenantId { get; set; }
        public string Url { get; set; }
        public string PortalUrl { get; set; }
        public AppInfo WebInfo { get; set; }
        public AppInfo SearchTenantInfo { get; set; }
        public AppInfo Linaker { get; set; }
        public MobileInfo MobileInfo { get; set; }
        public JicroInfo JicroInfo { get; set; }
        public PublicApiInfo PublicApiInfo { get; set; }
        public TwilioInfo TwilioInfo { get; set; }
        public Environment() { }
    }

    public enum InitBy
    {
        Db, Controller, Jicro
    }

    public class AppInfo
    {
        //public string Url { get; set; }
        public Credential Credential { get; set; }
        public List<Credential> Credentials { get; set; }
    }

    public class MobileInfo : AppInfo
    {
        public string? MobileWebApiUrl { get; set; }
        public string? MobileOfflineUrl { get; set; }
        public string? MobileWorkflowUrl { get; set; }
        public string? EnterpriseMobileOfflineUrl { get; set; }
        public IdentityServer? IdentityServer { get; set; }
        public LoginInfor? LoginInfor { get; set; }
        public CloudProvider? CloudProvider { get; set; }
        public string? BundleID { get; set; }
        public string? TrackerURL { get; set; }
    }

    public class PublicApiInfo : AppInfo
    {
        public string? Url { get; set; }
        public IdentityServer? IdentityServer { get; set; }
        public Guid TenantId { get; set; }
    }

    public class IdentityServer
    {
        public string? IdentityServerUrl { get; set; }
        public string? ClientSecrect { get; set; }
        public string? ClientId { get; set; }
        public string? GrantType { get; set; }
        public string? Scopes { get; set; }
    }

    public class LoginInfor
    {
        public string? ApplicationVersion { get; set; }
        public string? appKey { get; set; }
        public string? DeviceOSVersion { get; set; }
        public int? DeviceEnvironment { get; set; }
        public int? DeviceType { get; set; }
        public string? DeviceUUID { get; set; }
        public string? DeviceRegistrationCode { get; set; }
        public string? DeviceModel { get; set; }
        public bool CheckLoginType { get; set; }
        public string? ApplicationSource { get; set; }
    }

    public class JicroInfo
    {
        public string? ServiceBusNamespace { get; set; }
        public string? LicenseJicroserviceUrl { get; set; }
        public string? NotificationJicroserviceUrl { get; set; }
        public string? CoreJicroserviceUrl { get; set; }
        public string? UKCoreJicroserviceUrl { get; set; }
        public string? FileJicroserviceUrl { get; set; }
        public Guid TenantId { get; set; }
    }

    public class DbInfo
    {
        public string? LicenseConnectionString { get; set; }
        public string? BusinessConnectionString { get; set; }
    }

    public class MongoDbInfo
    {
        //public MongoDb MongoDb { get; set; }
        //public List<MongoDb> MongoDbList { get; set; }
    }

    public class TwilioInfo
    {
        public string? TwilioService { get; set; }
    }

    public class CloudProvider
    {
        public string? AndroidCloudURI { get; set; }
        public string? IOSCloudURI { get; set; }
        public string? AndroidAppPath { get; set; }
        public string? IOSAppPath { get; set; }
        public string? IOSKeychainsPath { get; set; }
    }

    public class Credential
    {
        public string? EmailAddress { get; set; }
        public string? Password { get; set; }
        public string? Region { get; set; }
        public string? PortalUser { get; set; }
        public string? Type { get; set; }
        public string? Platform { get; set; }
        public string? Device { get; set; }
    }
}
