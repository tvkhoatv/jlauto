﻿namespace JLAuto.Infrastructure.Models
{
    public class LockObject
    {
        public string LockState { get; set; } = "Locked";
    }
}
