﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace JLAuto.Infrastructure
{
    public class BaseApp : IDisposable
    {
        public virtual void Initialize(TestContext testContext) { }

        public virtual void CleanUp() { }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
