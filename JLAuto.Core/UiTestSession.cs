﻿using JLAuto.DI;
using JLAuto.Factory;
using JLAuto.Settings;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.IO;

namespace JLAuto
{
    public class UiTestSession
    {
        private IServiceProvider _provider = null!;

        public SessionSettings Settings => Resolve<SessionSettings>();

        public static UiTestSession Instance => new Lazy<UiTestSession>(() => new UiTestSession()).Value;

        private UiTestSession()
        {}

        public void Start(TestContext context)
        {
            _provider = ServiceRegistry.Register();
            if (!string.IsNullOrWhiteSpace(Settings.DownloadDirectory) && !Directory.Exists(Settings.DownloadDirectory))
            {
                Directory.CreateDirectory(Settings.DownloadDirectory);
            }
        }

        public void CleanUp()
        {
            if (Directory.Exists(Settings.DownloadDirectory))
            {
                Directory.Delete(Settings.DownloadDirectory, true);
            }

            //DriverFactory.Dispose();
        }

        public T Resolve<T>() where T : notnull
        {
            if (_provider == null)
            {
                throw new InvalidOperationException("The session is not started");
            }

            return _provider.GetRequiredService<T>();
        }
    }
}
