﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JLAuto.Settings
{
    public sealed class RuntimeSetting
    {
        public static TestContext? Context { get; set; }

        public RuntimeSetting() { }

        public RuntimeSetting(TestContext context)
        {
            Context = context;
        }

        public static RuntimeSetting Instance => new System.Lazy<RuntimeSetting>(() => new RuntimeSetting()).Value;

        public string GetProperty(string property)
        {
            return (string)Context?.Properties[property] ?? null;
        }

        public TestContext GetContext()
        {
            return Context;
        }
    }
}
