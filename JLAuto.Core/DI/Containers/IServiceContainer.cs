﻿using Microsoft.Extensions.DependencyInjection;

namespace JLAuto.DI.Containers
{
    public interface IServiceContainer
    {
        void Register(IServiceCollection collection);
    }
}
