﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace JLAuto.DI
{
    public class ServiceRegistry
    {
        //public static ServiceRegistry Instance => new Lazy<ServiceRegistry>(() => new ServiceRegistry()).Value;

        static ServiceRegistry()
        {
            _collection = new ServiceCollection();
        }

        private static ServiceCollection? _collection;
        private static ServiceProvider? _provider;

        public static ServiceCollection? GetService()
        {
            return _collection;
        }

        public static ServiceProvider? Register()
        {
            _provider = _collection?.BuildServiceProvider();

            return _provider;
        }

        public static T Resolve<T>() where T : notnull
        {
            if (_provider == null)
            {
                throw new InvalidOperationException("The provider is not started");
            }

            return _provider.GetRequiredService<T>();
        }
    }
}
