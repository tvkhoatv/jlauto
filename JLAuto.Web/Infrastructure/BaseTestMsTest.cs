﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

using JLAuto.DI;
using JLAuto.Factory;
using JLAuto.Web;
using JLAuto.Infrastructure.Variables;
using JLAuto.Web.Business;
using JLAuto.Core.Utils;
using JLAuto.Infrastructure.Models;
using DriverFactory = JLAuto.Factory.DriverFactory;
using BaseTest = JLAuto.Web.Infrastructure.Base.BaseTest;
using AppUi = JLAuto.Web.AppUi;

namespace JLAuto.Web.Infrastructure
{
    [TestClass]
    public abstract class BaseTestMsTest : BaseTest
    {
        protected BaseTestMsTest()
        {
        }

        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext context)
        {
            AppUi.Instance.Initialize(context);
            DriverFactory mainDriverFactory = ServicesCollection.Current.Resolve<DriverFactory>();

            var environment = AppConfigFactory.Instance;
            var Credentials = new List<Credential>();
            Credentials.AddRange(environment.WebInfo.Credentials);
            var Credential = Credentials.FirstOrDefault(x => 
                x.Type == AppConfigFactory.TenantType && 
                x.Region == AppConfigFactory.Region
            );

            //mainDriverFactory.InitDriver(AppConfigFactory.BrowserTypes.FirstOrDefault());
            mainDriverFactory.GetDriver();
            mainDriverFactory.NavigateToUrl(environment.Url);

            var loginPage = AppUi.Instance.LoadPage<LoginPage>();

            // Get cookies assign for webdriver
            loginPage.SignOn(credential: Credential)
                .VerifyAccountMenuPresent();

            AppConfigFactory.Url = environment.Url;
            AppConfigFactory.TenantType = Credential.Type;

            var initialCookies = mainDriverFactory.GetAllCookiesAsString();
            CacheManager.Add(AppConfigFactory.TenantType, initialCookies);

            mainDriverFactory.CloseDriver();
        }

        [TestInitialize]
        public async Task Initialize()
        {
            driverFactory = ServicesCollection.Current.Resolve<DriverFactory>();
            var cookies = CacheManager.Get<string>(AppConfigFactory.TenantType);

            if ( cookies == null )
                throw new System.Exception("The system can not initialized");

            driverFactory.GetDriver();
            driverFactory
                .NavigateToUrl(AppConfigFactory.Url)
                .SetCookies(cookies.ToString());

        }

        [TestCleanup]
        public async Task Cleanup()
        {
        }

        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {
            AppUi.Instance.CleanUp();
        }

        private System.Type GetCurrentExecutionTestClassType()
        {
            string className = TestContext.FullyQualifiedTestClassName;
            var testClassType = GetType().Assembly.GetType(className);

            return testClassType;
        }
    }
}
