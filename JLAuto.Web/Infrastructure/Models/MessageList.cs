﻿using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Office2010.Word;

namespace Infrastructure.Models
{
    public class MessageList
    {
        protected MessageList() { }

        public const string UserBlankError = "The Email field is required.";
        public const string PassBlankError = "The Password field is required.";
        public const string WrongUserPass = "The email address or password provided is incorrect.";
        public const string EmailBlank = "Email Address is required.";
        public const string EmailNotValid = "Please enter a valid email address.";
        public const string EmailSent = "Email has been sent";
        public const string NoMatch = "No matching results found";
        public const string NoResultFound = "No matching result found";
        public const string NoPOMatch = "There are no Purchase Orders on this Job which match your current search criteria.";
        public const string MessageDeletion = "The items were successfully deleted.";
        public const string ItemDetailNotExists = "Sorry this item no longer exists!";
        public const string ImportSuccess = "Your Import was successful! Imported information should now be available.";
        public static string SuccessBatchDeploy => " visit(s) has been deployed";
        public static string FilterApplied => "Filter Applied";
        public static string NoVisitDeploy => "No visit(s) to deploy";
        public const string SaveContacts = "Contact(s) saved successfully";
        public const string SaveContact = "Contact saved successfully";
        public const string DeleteContact = "Contact removed successfully";
        public const string SuccessAsset = "Assets has been saved successfully";
        public const string SuccessAssetApi = "The asset has been saved successfully";
        public const string SuccessUnrelateAssetApi = "Asset has successfully been unrelated from master asset and system";
        public const string AddNote = "Note added successfully";
        public const string EditNote = "Note updated successfully";
        public const string DeleteNote = "Note removed successfully";
        public const string UpdateCompany = "Company updated successfully";
        public const string UpdateCompanyLogo = "Company logo successfully uploaded.";
        public const string PaymentDeletion = "Payment has been successfully deleted";
        public const string PaymentSaved = "Payment has been successfully saved";
        public const string TransferNoteError = "Tranfer Note File is required";
        public const string QuantityDecantError = "Quantity must be greater than zero";
        public const string DestinationGasCylinderError = "The Destination Cylinder field is required.";
        public const string MessageInvalidFile = "Invalid file added";
        public const string MessageSupplierInvoiceDeletion = "Purchase Order Supplier Invoice has been successfully deleted";
        public const string OrderedByColumn = "Table settings has been saved.";
        public const string NoExpense = "No expenses associated with this visit.";
        public const string NoPart = "No parts used associated with this visit.";
        public const string NoPartRequired = "No parts required associated with this visit.";
        public const string OverlappedTime = "The edited time would overlap in the timesheet and would not be synced with Timesheet,Are you sure wish to update the time.";
        public const string UnsyncedTimesheet = "The timesheet cannot be synced with the job costs as they have been invoiced. Please click Yes to save the timesheet only?";
        public static string LogJobFromPortal(string jobNo, string portalFullName)
            => $"A new Job ({jobNo}) has been logged from the Customer Portal by {portalFullName}";
        public const string TapNumberError = "Tap Number must be between 1 and 2147483647";
        public const string NoTapAndBrand = "No Taps assigned to Asset";
        public static string VisitNote(string engineer, string jobNo)
            => $"{engineer} has an important note about job {jobNo}";
        public static string UpdateVisitNote(string engineer, string jobNo)
            => $"{engineer} has updated an important note about job {jobNo}";
        public static string CompleteJobOnPortal(string jobNo, string portalFullName)
            => $"A Job ({jobNo}) has been marked as completed in the Customer Portal by {portalFullName}";

        public const string ApproveVisit = "A visit has been approved from the Customer Portal. Please click the title of this notification to view your visit.";
        public const string DuplicateStockVehicle = "Registration Number already exists.";
        public const string DuplicateStockLocation = "Location name already exists.";
        public const string DuplicateStockRackShelf = "Rack/Shelf name already exists.";
        public const string QuotePayment = "Payments - Quote(s)";
        public const string PaymentSuccessfull = "Payment successful.";
        public const string InvoicePayment = "Payments - Invoice(s)";
        public const string UnDeleteMessage = "You cannot delete this Job as it has been added to a Customer Grouped Invoice. Please delete these lines if you would like to delete this Job.";
        public const string ImportCustomer = "Import - Customer(s)";
        public const string ImportSite = "Import - Site(s)";
        public const string ImportServiceType = "Import - Service Type(s)";
        public const string ImportSupplier = "Import - Supplier(s)";
        public const string ImportSubcontractor = "Import - Subcontractor(s)";
        public const string ImportAsset = "Import - Site Asset(s)";
        public const string ImportSORItem = "Import - Schedule Of Rates Item(s)";
        public const string ImportStatusSuccess = "Success";
        public const string NoMatchItemInList = "No match found, add this item to list";
        public const string MsgSaveEmailTemplate = "Email Template saved successfully";
        #region Cgroup invoice
        public const string CgroupInvoiceJobError = "Please select at least one Job to Invoice";
        public const string CgroupInvoiceCustomerError = "Customer does not exist";
        public const string CgroupInvoiceCustomerRequired = "the customer field is required.";
        public const string CgroupInvoiceNoCostError = "Cannot create empty Customer Grouped Invoice";
        #endregion
        public const string InvoiceBlankDateRaised = "Date Raised is Required";
        public const string InvoiceBlankPaymentDueDate = "Payment Due Date is Required";
        public const string JobNotExists = "Job does not exist";
        public const string DeleteGasCylinder = "Item Successfully Deleted";
        public const string InvoiceNotExist = "Invoice does not exist";
        public const string CreditWithoutCreditNotes = "Cannot approve credit. No credit reason has been entered";
        public const string DeleteItem = "Item Successfully Deleted";
        public const string AddVisitTime = "Visit time has been successfully created";
        public const string UpdateVisitTime = "Visit time has been successfully updated";
        public const string DeleteVisitTime = "Visit time has been successfully deleted";
        public const string AddExpenseVisit = "Visit expense has been successfully created";
        public const string UpdateExpenseVisit = "Visit expense has been successfully updated";
        public const string DeleteExpenseVisit = "Visit expense has been successfully deleted";
        public const string AddPartVisit = "Visit part has been successfully created";
        public const string UpdatePartVisit = "Visit part has been successfully updated";
        public const string DeletePartVisit = "Visit part has been successfully deleted";
        public const string DeletedLineSuccessfully = "Deleted line successfully";
        public const string UpdatedLineSuccessfully = "Updated line successfully";
        public const string CreatedLineSuccessfully = "Created line successfully";
        public const string UpdateChangesSuccessfully = "Your changes has been successfully updated.";
        public const string SelectOption = "Please select an option...";

        public const string SystemId = "System ID is Required";
        public const string AutoReorderStockRecord = "Auto Stock Purchase Order successfully created! Please click the title of this notification to view the detail.";

        public const string VehiclRegNoRequired = "The Registration Number field is required.";
        public const string VehicleMileageValidation = "The field Last Known Mileage must be between 0 and 2147483647.";
        public const string LocationNotExist = "Location does not exist";
        public const string LocationValidation = "The Location Name field is required.";
        public const string LocationHaveNoStock = "The selected location does not have any stock records";
        public const string LocationHaveOpenStockTake = "An Open Stock Take exists for this Location";

        #region PPM 
        public const string RenewPpmNotification = "PPM Contract successfully renewed! Please click the title of this notification to view your new contract.";
        public const string MessageClonePpmInvoice = "Invoice has successfully been cloned";
        public const string PPMContractCanceled = "This PPM Contract has been cancelled.";
        public const string PPMVisitDeleted = "PPM Visit has been deleted";
        public const string PPMQuoteImportValidationValid = "Your PPM quote schedule import is valid";
        public const string PPMContractImportValidationValid = "Your Import was successful! Imported information should now be available.";
        public const string PPMImportValidationInvalid = "Your Import has failed validation. Please visit Settings > Import History to find out more information.The option to import the schedule again is available on the PPM Quote/Contract detail page";
        public const string PPMQuoteReimport = "Schedule already exists, do you want to import the schedule again? The existing schedule will be overridden.";
        public const string PPMContractReimport = "All the existing data will be delete if re-import the schedule. Are you sure you want to import?";
        public const string PPMContractSuspended = "This PPM Contract has been suspended.";
        public const string PPMContractSuspendedRelatedQuote = "This Quote is suspended due to the PPM contract has been suspended. Click here for more information.";
        public const string PPMContractSuspendedPO = "This PO is suspended due to the PPM contract has been suspended. Click here for more information.";
        public const string PPMContractSuspendedJob = "This job is suspended due to the PPM contract has been suspended.";
        public const string PPMQuoteEmailSent = "PPM Quote email has been sent successfully!";
        public static string PPMQuoteApproved(string user, string datetime) => $"APPROVED: This PPM Quote has been approved by {user} on {datetime}";
        public const string PPMQuoteRejectedFromPortal = "REJECTED : Rejected from portal, see notes.";
        #endregion

        #region Stock
        public const string StockTakeWarning = "Please Note: Any records without a New Quantity entered will not be modified";
        public const string StockTakeLocationRequired = "The Location field is required.";
        public const string StockTakeNotesRequired = "The Notes field is required.";
        public const string PartRequired = "The Part field is required.";
        public const string EquipmentRequired = "The Equipment field is required.";
        public const string LocationRequired = "The Location field is required.";
        public const string RackShelfRequired = "The Rack/Shelf field is required.";
        public const string CreateRackRequireLocation = "To add a Rack/Self, you must first select a Stock Location";
        #endregion

        public const string CannotSaveFgasTransactionReason = "Could not save Addition/Removal Reason. Please try again.";
        public const string CannotSave = "Could not save document type. Please try again.";

        public const string CannotSaveQuoteRejectReason = "Could not save Quote Reject Reason. Please try again.";

        public const string SetPasswordSuccessful = "Your password has been set";
        public const string ForgotPasswordSuccessful = "Password Updated";
        public const string DescriptionRequired = "Description is required";
        public const string DescriptionRequiredOnLogJob = "The Description field is required.";
        public const string CustomerNameRequired = "Customer Name is Required";
        public const string SiteNameRequired = "Site Name is Required";
        public const string CustomerRequired = "Customer is Required";
        public const string CustomerRequiredOnLogJob = "The Customer field is required.";
        public const string SiteRequired = "Site is Required";
        public const string SiteRequiredOnLogJob = "The Site field is required.";
        public const string NeedSelectCustomerToAddSiteOTF = "To add a Site, you must first select a Customer";
        public const string IndustrySaved = "The Industry field is required.";
        public const string QuantityRequired = "Asset Quantity is Required";
        public const string DescriptionFieldRequired = "The Description field is required.";
        public const string NoItemsFound = "Sorry, no matching options.";
        public const string NoDataFound = "No data found.";
        public const string BatchVisitMove = "Your request for moving visits in bulk is complete.";
        public const string CannotDeleteTeam = "The Engineer Team cannot be deleted because a visit is in progress. Please complete the current team visit before deleting the engineer team.";
        public const string CannotChangeLeader = "The Lead engineer cannot be removed/changed because a visit is in progress. Please complete the current team visit before removing/changing the lead engineer";
        public const string NoSignaturesAssociatedWithThisVisit = "No signatures associated with this visit.";



        #region Gas cylinder
        public const string SupplierRequired = "Supplier is required";
        public const string SerialNoRequired = "The Serial No. field is required.";
        public const string DatePurchaseRequired = "The Date of Purchase field is required.";
        #endregion

        #region Tax rate
        public const string TaxCodeRequired = "The Code field is required.";
        public const string TaxDescriptionRequired = "The Description field is required.";
        #endregion

        #region Equipment
        public const string EquipmentNumberRequired = "Model Number is Required";
        public const string EquipmentQuantityRequired = "Quantity is Required";
        public const string EquipmentPriceRequired = "Price is Required";
        #endregion

        #region Billing
        public const string NameRequired = "Name is Required";
        public const string Address1Required = "Address Line 1 is Required";
        public const string AddressRequired = "Address is Required";
        public const string PostcodeRequired = "Postcode is Required";
        #endregion

        #region Refcom enable
        public const string RefcomMemberRequired = "The Refcom Member Number field is required.";
        public const string RefcomWasteNoteRequired = "The Waste Note Number field is required.";
        public static string MaxlengthField(string field, int max)
            => $"The {field}RegistrationNumber field may not be greater than {max} characters.";
        #endregion

        #region Part Library
        public const string CannotDeletePartLibrary = "Can not delete default part library.";
        public const string PartLibraryNameRequired = "The Name field is required.";
        #endregion

        #region Subcontractor
        public const string SubContractorNameRequired = "The Name field is required.";
        public const string SubContractEmailRequired = "The Email field is required.";
        #endregion

        #region Trade
        public const string RoleNameTradeRequired = "The Role Name field is required.";
        #endregion

        #region Payband
        public const string ColorPaybandRequired = "The Colour field is required.";
        #endregion

        #region Form
        public const string FormEdited = "Form successfully saved";
        public const string FormDeployed = "Form deploy successfully";
        public const string FormUndeployed = "This Custom Form has been undeployed";
        #endregion

        public const string QuoteRejectReasonRequired = "The Reason field is required.";
        public const string EditEngineerSuccess = "Edit Engineer Successful.";

        public const string MaximumJobType = "Job Type cannot be created as maximum number allowed has been reached";

        public const string ColourRequired = "Colour is Required";
        public const string TitleRequired = "Title is Required";

        #region Quote
        public const string QuoteSend = "This Quote has been sent";
        public static string ApproveQuoteOnPortal(string quoteNo, string portalFullName = null)
            => portalFullName != null ? $"A Quote ({quoteNo}) has been approved from the Customer Portal by {portalFullName}" : $"A Quote ({quoteNo}) has been approved from the Customer Portal";
        public static string RejectQuoteOnPortal(string quoteNo, string portalFullName)
            => $"A Quote ({quoteNo}) has been rejected from the Customer Portal by {portalFullName}";
        public static string QuoteApproved(string user, string datetime) => $"APPROVED: This Quote has been approved by {user} on {datetime}";
        public static string QuoteUpgraded(string user, string datetime) => $"UPGRADED: This Quote has been upgraded by {user} on {datetime}";
        #endregion

        #region Task
        public const string TaskRequired = "The Task field is required";
        public const string SummaryRequired = "The Summary field is required";
        public const string SubTaskDurationValidation = "The Duration field must be numeric and may contain decimal points";
        #endregion

        #region JobType
        public const string JobTypePrefixRequired = "The Prefix field is required.";
        public const string JobTypeNexNumberRequired = "The Next Number field is required.";
        #endregion

        #region ServiceType
        public const string TitleServiceRequired = "The Title field is required.";
        #endregion

        #region SOR
        public const string DeleteSORConfirmMessage = "Are you sure you would like to delete this line?";
        public const string DefaultSORAgainstCustomer = "This Library is currently set as the Default Library against one or more Customers. Please un-assign this Library from the Customer(s) before marking as Inactive.";
        public const string CannotDeleteDefaultSORLibrary = "Cannot Delete a Default Schedule of Rates Library";
        #endregion

        #region Dashboard
        public const string ChangeSourceSuccessfully = "Changes saved succesfully";
        public const string PublishDashboardSuccessfully = "Dashboard Successfully Published";
        public const string DeleteDashboardUserSuccessfully = "Dashboard user successfully removed";
        #endregion

        #region Quick Filters
        public const string QuickFilterNotExist = "Quick filter not found.";
        public const string NoQuickFilterCreated = "You have not currently created any Quick Filters";
        #endregion

        #region Subcontractor Job
        public static string SubcontractorJobNoti(string jobId, string action, string subcontractorName)
            => $"An allocated job {jobId} has been {action.ToLower()} by the Subcontractor {subcontractorName}";

        #endregion

        #region Recurring Job
        public const string RecurringWaiting = "The job Recurrence is being run";
        public static string RecurringCompleted(string jobId) => $"The job has already been recurred {jobId}";
        public const string RecurringCancelled = "This job will no longer recur";
        public static string NewRecurJobCreated(string jobId)
            => $"The job has recurred successfully - Please click on the {jobId} to view the more details";
        #endregion

        #region Company Setup
        public const string EndTimeMustbeAfterStartTime = "End time must be after start time.";
        #endregion

        #region Tags
        public const string TagUpdatedSuccessfully = "Tag has been successfully updated";
        #endregion
    }
    public class MobileMessageList
    {
        public const string DateFormatOnVisit = "dd/MM/yyyy - HH:mm";
        public const string AndroidNoRecordFound = "No record found";

        #region
        public const string IncorrectUsernamePassword = "The username or password provided is incorrect.";
        public const string UnableToPerformRequestedOperation = "Unable to perform requested operation.";
        #endregion

        #region Gas Cylinder
        public const string AndroidDateOfPurchaseRequire = "Please select date of purchase";
        public const string AndroidRefrigerantTypeRequire = "Please select a valid gas type from dropdown";
        public const string AndroidSerialRequire = "Please enter serial number";
        public const string AndroidSupplierRequire = "Please select a valid supplier from dropdown";
        public const string AndroidWeightRequire = "Please enter valid weight.";
        public const string AndroidCurrentWeightRequire = "Please enter valid current weight.";
        public const string AndroidEngineerRequire = "Please select Engineer from dropdown";

        public const string AndroidEngineerSignaturRequire = "Signature is required.";
        public const string AndroidWasteReceiverSignature = "Signature is required.";
        public const string AndroidWasteReceiverRequire = "Waste Receiver's name is required";
        #endregion

        #region Visit notification
        public static string NewVisitNotification(string JobNo)
            => $"{JobNo} has been allocated to you.";
        public static string UpdatedVisitNotification(string JobNo)
            => $"{JobNo} has been updated.";
        public static string CancelledVisitNotification(string JobNo)
            => $"{JobNo} has been cancelled.";
        #endregion

        #region Visit detail
        public const string CopiedToClipboard = "Copied to clipboard!";
        public static string JobNumberHasBeenCopied(string JobNo)
            => $"Job Number {JobNo} has been copied to clipboard.";
        public const string SuspendedPPMWarning = "This PPM contract has been suspended";
        #endregion

        #region Accept All
        public static string AcceptAllMessage(int NumberOfVisit)
            => $"You have {NumberOfVisit} new visit";
        public const string AcceptAllConfirmation = "Are you sure you wish to Accept all Visits?";
        #endregion

        #region Visit status
        public const string NewStatusDateMustBeGreaterThanThePreviousDate = "New status date must be greater than the previous date.";
        public const string LeaveSiteReasonMissing = "Leave site reason missing.";
        public const string CancelVisitAlertPopup = "This visit has been cancelled by your admin. You cannot perform any more actions on this visit.";
        public const string InvalidMileageEntered = "Invalid mileage entered.";
        public const string PleaseEnterAValidMileageValue = "Please enter a valid Mileage value.";
        public const string MileageHasBeenCalculated = "Mileage has been calculated based on GPS coordinates. Please update if needed.";
        #endregion

        #region Job Process Warning
        public const string JobProcessWarningTitle = "Job Process Warning!";
        public static string JobProcessWarningMessage(string jobNumber)
                => $"You are already active on Job {jobNumber}.";
        public const string JobProcessWarningConfirmation = "Are you sure you wish to proceed? Doing so will lead to overlapping timesheet entries";
        public const string ProcessAnyway = "Process Anyway";
        #endregion

        #region Restrict Engineer Travelling 
        public const string RestrictTitle = "Alert";
        public const string RestrictMessage = "Your previous visit is still in progress. Please close the visit as ‘Complete’ or ‘Left site’, before starting a New Visit.";
        #endregion

        #region Error messages for report
        public const string LoginError = "Login Failed!";
        public const string LoginLoadingForever = "Timeout when login account, please check again!";
        public static string VisitNotReceived(string jobId)
            => $"Timeout for receiving visit {jobId}";
        public const string ListOfVisitNotReceived =  "Timeout for receiving list of visit";
        public static string TimeouRececingCancelNoti(string jobId)
            => $"Timeout for receiving cancel visit notification {jobId}";
        public static string DatePickerIsIncorrect(string expected, string actual)
            => $"DateTime is updated after clicking cancel button. Expected: {expected} - Actual: {actual}";
        public static string SyncAppIsNotProcessedWithVisitId(int visitId)
            => $"Sync app is not processed with visitId = {visitId}";
        public static string VisitActionIsNotSendToMongoWithVisitId(int visitId)
            => $"Visit action is not sent to mongo with visitId = {visitId}";
        public const string VisitStatusIsNotCovered = "Visit status is not covered";
        public const string ReasonIsMissing = "Please provide reason for these status: abort, leave site, reject, no access";

        public static string TrackerSettingNotFound(string settingName) => $"Tracker Setting Not Found: {settingName}";
        #endregion

        #region Invalid date selection
        public const string InvalidDateTitle = "Invalid Dates";
        public const string InvalidDateMessage = "The End Date must be after the Start Date";
        #endregion

        #region Complete visit
        public const string IncompleteAssetWaringTitle = "Not all assets have been completed. Do you want to complete them now?";
        public const string IncompleteAssetWaringYes = "Yes, Continue";
        public const string IncompleteAssetWaringNo = "No, Go To Assets";

        public const string IncompleteTaskWaringTitle = "Not all tasks have been completed. Are you sure you want to complete job?";
        public const string IncompleteTaskWaringYes = "Yes, Continue";
        public const string IncompleteTaskWaringNo = "No, Go To Tasks";
        public static string CompleteVisitStep(int step, int totalStep)
                => $"Complete Visit {step}/{totalStep}";
        public const string NoAttachments = "No Attachments have been added";
        public const string NoParts = "No Parts have been added";
        public const string NoExpenses = "No Expenses have been added";
        public const string YouHaveNotAddedAnyRequiredPartsToThisJob = "You have not added any required parts to this job.";
        public const string ThereArePartsRequiredOnThisJob = "There are Parts Required on this job";
        public const string PartsRequired = "Parts required";
        public const string RevisitReasonMissing = "Revisit reason missing.";
        public const string NoNotesEntered = "No notes entered.";
        public const string SignatureIsRequired = "Signature is required.";
        public const string PleaseProvideEngineerSignature = "Please provide Engineer's Signature";
        public const string ThereAreIncompletedAssetsOnThisJob = "There are incompleted assets on this job";
        public const string IncompleteAssets = "Incomplete Assets";
        public const string PleaseProvideRevisitReason = "Please provide revisit reason";
        #endregion

        #region Leave site
        public static string LeaveSiteVisitStep(int step)
               => $"Visit Action - Leave Site {step}/3";
        #endregion

        #region Actions on app
        public const string CannotCloseAppMultiTask = "Cannot close app in multi task!";
        public static string ImageNotFound(string imageName)
            => $"Image not found: {imageName}";
        public static string ElementNotFoundByImage(string imageName)
            => $"Element not found by image {imageName}!";
        #endregion

        #region Visit Status history
        public const string NewJob = "New Job";
        public const string JobAccept = "Job Accept";
        public const string JobRejected = "Job Rejected";
        public const string Travel = "Travel";
        public const string OnSite = "On Site";
        public const string JobCompleted = "Job Completed";
        public const string LeftSite = "Left Site";
        public const string TravelAbandoned = "Travel Abandoned";
        public const string NoAccess = "No Access";
        public const string JobAborted = "Job Aborted";
        public const string JobCancelled = "Job Cancelled";
        public const string TravelHome = "Travel Home";
        public const string TravelingHome = "Travelling Home";
        public const string AtHome = "At Home";
        public const string JobUpdated = "Job Updated";
        #endregion

        #region Visit Other detail
        public const string NoJobNotesToDisplay = "No Job notes to display";
        public const string NoJobAttachmentsToDisplay = "No job attachments to display";
        public const string NoQuoteNotesToDisplay = "No quote notes to display";
        public const string NoQuoteAttachmentsToDisplay = "No Quote attachments to display";
        public const string NoQuoteLinesToDisplay = "No Quote lines to display";
        public const string NoSiteNotesToDisplay = "No site notes to display";
        public const string NoSiteAssetsToDisplay = "No site assets to display";
        public const string NoSiteHistoryToDisplay = "No Site history to display";
        public const string NoQuotesHistoryToDisplay = "No Quotes history to display";
        public const string NoSiteAttachmentsToDisplay = "No site attachments to display";
        public const string NoOtherVisitsForThisJobToDisplay = "No other visits for this job to display";
        #endregion
    }
    
    public class MobileElementList
    {
        #region Common
        public const string Cancel = "Cancel";
        public const string Continue = "Continue";
        public const string Next = "Next";
        public const string Back = "Back";
        public const string YES = "YES";
        public const string NO = "NO";
        public const string Save = "Save";
        public const string Dismiss = "Dismiss";
        public const string Confirmation = "Confirmation";
        public const string ViewLocationOnMap = "View Location On Map";
        public const string ContactSite = "Contact Site";
        #endregion

        #region Visit Status
        //Title
        public const string VisitActionReject = "Visit Action - Reject";
        public const string VisitActionAbort = "Visit Action - Abort";
        public const string VisitActionAbandon = "Visit Action - Abandon";
        public const string VisitActionNoAccess = "Visit Action - No Access";
        public const string VisitActionTravelHome = "Visit Action - Travel Home";
        public static string AtHomeStep(int step)
            => $"Visit Action - At Home {step}/2";
        public const string VisitActionAccept = "Visit Action - Accept";
        public const string VisitActionTravel = "Visit Action - Travel";
        public const string VisitActionOnSite = "Visit Action - On Site";

        //Change status button
        public const string RejectVisit = "Reject Visit";
        public const string AbortVisit = "Abort Visit";
        public const string Abandon = "Abandon";
        public const string NoAccess = "No Access";
        public const string TravelHome = "Travel Home";
        public const string TravelToSite = "Travel To Site";

        //Visit status
        public const string Travelling = "Travelling";
        public const string OnSite = "On Site";
        public const string Complete = "Complete";
        public const string QuickComplete = "Quick Complete";
        public const string LeaveSite = "Leave Site";
        public const string Reject = "Reject";
        public const string AbandonTravel = "Abandon Travel";
        public const string TravellingHome = "Travelling Home";
        public const string Accept = "Accept";

        public const string LeaveSiteReason = "Leave Site Reason";
        public const string RejectReason = "Reject Reason";
        public const string AbortReason = "Abort Reason";
        public const string AbandonTravelReason = "Abandon Travel Reason";
        public const string NoAccessReason = "No Access Reason";
        public const string EnterYourReason = "Enter your reason...";

        //Quick complete
        public const string TextHere = "Text here...";
        public const string Clear = "Clear";
        public const string Assets = "Assets";
        public const string TakeMeToTheAssetPage = "Yes, take me to Assets page";
        //Visit status - Complete
        public const string AddAttachments = "Add Attachments";
        public const string Add = "Add";
        public const string AddParts = "Add Parts";
        public const string AddExpenses = "Add Expenses";

        public const string Parts = "Parts";

        public const string AnyRecommendation = "Any Recommendation?";
        public const string RevisitReason = "Revisit Reason";
        public const string SelectAnOption = "Select an option...";

        public const string AddVisitPublicNote = "Add Visit Public Notes";
        public const string VisitPublicNotes = "Visit Public Notes";

        public const string CustomerName = "Customer Name";
        public const string Position = "Position";
        public const string CustomerSignature = "Customer Signature";
        public const string EngineerName = "Engineer Name";
        public const string EngineerSignature = "Engineer Signature";
        #endregion

        #region Visit Listing
        public const string AcceptVisit = "Accept Visit";
        public const string AcceptAll = "Accept All";
        #endregion

        #region Visit Other detail
        public const string PublicNotes = "Public Notes";
        public const string PrivateNotes = "Private Notes";
        public const string JobDetail = "Job Detail";
        public const string Description = "Description";
        public const string JobType = "Job Type";
        public const string JobOwner = "Job Owner";
        public const string JobOwnerContact = "Job Owner Contact";
        public const string TaskType = "Task Type";
        public const string Status = "Status";
        public const string Priority = "Priority";
        public const string OrderNo = "Order No.";
        public const string ReferenceNo = "Reference No.";
        public const string Customer = "Customer";
        public const string Site = "Site";
        public const string Address = "Address";
        public const string Contact = "Contact";
        public const string Telephone = "Telephone";
        public const string SecondaryTelephone = "Secondary Telephone";
        public const string DateLogged = "Date Logged";
        public const string DateCompleted = "Date Completed";
        public const string EstAppointment = "Est Appointment";        

        public const string Public = "Public";
        public const string Private = "Private";

        public const string JobNotes = "Job Notes";
        public const string JobVisits = "Job Visits";
        public const string JobAttachments = "Job Attachments";
        public const string Logbook = "Logbook";

        public const string NoNote = "No note";
        public const string NoRecommendation = "No recommendation";

        public const string QuoteDetail = "Quote Detail";
        public const string QuoteNotes = "Quote Notes";
        public const string QuoteAttachments = "Quote Attachments";
        public const string QuoteLines = "Quote Lines";
        public const string CustomerDetail = "Customer Detail";
        public const string SiteDetail = "Site Detail";


        public const string SiteNotes = "Site Notes";
        public const string SiteAssets = "Site Assets";
        public const string SiteJobsHistory = "Site Jobs History";
        public const string SiteQuotesHistory = "Site Quotes History";
        public const string SiteAttachments = "Site Attachments";
        public const string SiteLogbook = "Logbook";

        public const string Active = "Active";

        public const string Search = "Search...";
        public const string All = "All";


        public const string OtherVisitsOfThisJob = "Other Visits of This Job";
        public const string VisitStatusHistory = "Visit Status History";
        #endregion

        public const string VisitDetails = "Visit Details";

        #region Login 
        public const string WelcomeTo = "Welcome to";
        public const string ServiceManagementSoftware = "Service Management Software";
        public const string PleaseLogin = "If You already have your username and password please login.";
        public const string LogIn = "Log In";
        public const string OR = "OR";
        public const string CreateAnAccount = "Create an account and start your 30-day trial (no card details)";
        public const string Free30DayTrial = "Free 30-Day Trial";
        public static string Copyright(int year) => $"Copyright © {year}";
        public const string Joblogic = "Joblogic";


        public const string Register = "Register";
        public const string StartFREE30DayTrial = "Start FREE 30-Day Trial";
        public const string joblogiccom = "joblogic.com";
        public const string FieldServiceManagementSofrwe = "FIELD SERVICE MANAGEMENT SOFTWARE";

        public const string AccountForgottenPassword = "/Account/ForgottenPassword";
        public const string ForgotYourPassword = "Forgot Your Password?";
        #endregion

    }
}
