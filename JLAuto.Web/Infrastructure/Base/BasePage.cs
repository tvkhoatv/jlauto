using JLAuto.Factory;
using JLAuto.Infrastructure.Base;
using JLAuto.Infrastructure.Models;
using JLAuto.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace JLAuto.Base
{
    public abstract partial class BasePage<P, M>
        where P : BasePage<P, M>
        where M : IModel
    {
        protected WaitHelper waitHelper { get; set; }
        protected Keywords keywords { get; set; }
        protected DriverFactory _driverFactory { get; set; }
        protected TestUtil _testUtil { get; }
        protected IWebDriver _driver { get; set; }
        protected static string BaseUrl => ConfigurationService.GetSection("Url");

        protected bool _isOtf;
        protected M _model;

        public BasePage()
        {
            _driver = _driverFactory.GetDriver();
            PageFactory.InitElements(_driver, this);
        }

        public BasePage(DriverFactory driverFactory)
        {
            _driverFactory = driverFactory;
            waitHelper = new WaitHelper(driverFactory);
            keywords = new Utils.Keywords(driverFactory);
            _driver = _driverFactory.GetDriver();
            PageFactory.InitElements(_driver, this);
        }

        public BasePage(DriverFactory driverFactory, TestUtil testUtil) : this(driverFactory)
        {
            _driverFactory = driverFactory;
            _testUtil = testUtil;
        }

        public virtual string Url { get; set; }

        #region Declare elements

        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'_listbox')][@aria-hidden='false']/li[1]")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'multiselect--active')]//li[not(contains(@style,'display: none'))][1]")]
        protected IWebElement Lbl_FilteredDropdownItem { get; set; }

        [FindsBy(How = How.XPath, Using = "//ul[@role='listbox'][@class='vs__dropdown-menu']")]
        [FindsBy(How = How.XPath, Using = "//ul[@role='listbox'][contains(@class,'vs__dropdown-menu')]")]
        protected IWebElement Lbl_VueFilteredDropdownItem { get; set; }

        [FindsBy(How = How.XPath, Using = "//ul[@role='listbox'][@class='vs__dropdown-menu']")]
        [FindsBy(How = How.XPath, Using = "//ul[@role='listbox'][contains(@class,'vs__dropdown-menu')]")]
        protected IList<IWebElement> Lbl_VueFilteredDropdownItems { get; set; }


        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'__option-0')]")]
        protected IWebElement Lbl_VueFilteredDropdownItem2 { get; set; }

        protected IWebElement FindElementById(string id)
        {
            WebDriverWait wait = new WebDriverWait(_driver, new System.TimeSpan(0, 0, 10));
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.Id(id)));
        }

        protected IWebElement FindElementByXpath(string xpath)
        {
            //WebDriverWait wait = new WebDriverWait(_driver, new TimeSpan(0, 0, 10));
            //return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath(xpath)));
            return _driver.FindElement(By.XPath(xpath));
        }

        protected IList<IWebElement> FindElementsByXpath(string xpath)
        {
            //WebDriverWait wait = new WebDriverWait(_driver, new TimeSpan(0, 0, 10));
            //return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath(xpath)));
            return _driver.FindElements(By.XPath(xpath));
        }

        protected IWebElement Txt_CustomDropdownSearch(string label)
            => FindElementByXpath($"//*[label[contains(normalize-space(.),'{label}')]]//input[contains(@class,'multiselect__input')]");

        protected IWebElement Txt_CustomDropdownValue(string label)
            => FindElementByXpath($"//*[label[contains(normalize-space(.),'{label}')]]//span[@class='multiselect__tag']");

        protected IWebElement Lbl_SelectedCustomDropdown(string nameAttValue)
            => FindElementByXpath($"//*[contains(@name,'{nameAttValue}')]//*[contains(@class,'selected')]");

        protected IWebElement Txt_CustomDropdown(string nameAttValue, string nameAtt = "name")
            => FindElementByXpath($"//*[contains(@{nameAtt},'{nameAttValue}')]//input[@type='search']");

        protected IWebElement Txt_CustomDropdownFromLabel(string nameAttValue)
            => FindElementByXpath($"//*[label[contains(normalize-space(.),'{nameAttValue}')]]//input[@type='search']");

        protected IWebElement Lbl_CustomFilteredDropdownItem(string label)
            => FindElementByXpath($"//*[label[contains(normalize-space(.),'{label}')]]//ul[contains(@class,'multiselect__content')]");

        protected IWebElement Lbl_CustomFilteredDropdownItem(string label, string text)
        {
            IList<IWebElement> elements;
            elements = FindElementsByXpath($"//*[label[contains(normalize-space(.),'{label}')]]//ul[contains(@class,'multiselect__content')]//*[@class='multiselect__element']//*[contains(text(),'{text}')]");
            return elements.FirstOrDefault();
        }

        [FindsBy(How = How.TagName, Using = "h3")]
        protected IWebElement Lbl_Title { get; set; }

        [FindsBy(How = How.Name, Using = "Number")]
        protected IWebElement Txt_ModelNumber { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@name = 'Quantity1' or @name = 'Quantity' or @id = 'Quantity']")]
        [FindsBy(How = How.Name, Using = "Quantity-0")]//for SOR line
        protected IWebElement Txt_Quantity { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@name , 'Quantity') or @id = 'Quantity']")]
        protected IList<IWebElement> Txt_Quantity_List { get; set; }


        [FindsBy(How = How.Name, Using = "PricePerUnit")]
        protected IWebElement Txt_PricePerUnit { get; set; }

        [FindsBy(How = How.Name, Using = "SellPerUnit")]
        protected IList<IWebElement> Txt_SellPerUnit { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@name = 'Model' or @id = 'Model' or @id='AssetModel']")]
        protected IWebElement Txt_Model { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@name = 'Make' or @id = 'Make' or @id = 'AssetMake']")]
        protected IWebElement Txt_Make { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='modal fade in']//*[contains(@class,'jl-button-save')]")]
        [FindsBy(How = How.XPath, Using = "(//*[@class='modal-content']//*[contains(@class,'SubmitButton')])[last()]")]
        protected IWebElement Btn_SaveOnModal { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-button-cancel') or contains(@id,'btnCancel')]")]
        protected IList<IWebElement> Btn_CancelOnModal { get; set; }

        [FindsBy(How = How.Name, Using = "LJCustomReference")]
        [FindsBy(How = How.Name, Using = "CustomReference")]
        protected IWebElement Txt_CustomReference { get; set; }

        //[FindsBy(How = How.XPath, Using = "(//input[@type='file'])[2]")]
        protected IWebElement Txt_File
        {
            get
            {
                IList<IWebElement> elements = FindElementsByXpath("//input[@type='file']");
                if (elements.Count > 1)
                {
                    return elements[1];
                }
                else
                {
                    return elements.Last();
                }
            }
        }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal')][contains(@style,'display')]//*[text()='Yes']/parent::*")]
        [FindsBy(How = How.Id, Using = "modalConfirmYes")]
        protected IWebElement Btn_ConfirmYesOnModal { get; set; }

        [FindsBy(How = How.Id, Using = "modalConfirmNo")]
        protected IWebElement Btn_ConfirmNoOnModal { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'toast-message')]")]
        protected IWebElement Lbl_Message { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'toast-message')]")]
        protected IList<IWebElement> Lbl_Messages { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='toast-close-button']")]
        protected IList<IWebElement> Btn_ToastMessageClose { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@style='display: block;']//*[contains(@class,'jl-dots')]")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'dots-vertical')]")]
        protected IWebElement Btn_ActionMenu { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='responsive-menu']//*[contains(@class,'dots-vertical')]")]
        protected IWebElement Btn_ResponsiveActionMenu { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'share-variant')]")]
        protected IWebElement Btn_Share { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'share-button')]//a[@data-toggle='dropdown']")]
        protected IWebElement Btn_ShareOptions { get; set; }

        protected IWebElement Btn_AdditionalMenu(string buttonName)
            => FindElementByXpath($"//*[contains(@class,'additional-menu')]//*[.='{buttonName}']");

        protected IWebElement Btn_AdditionalMenuDropdown(string buttonName)
            => FindElementByXpath($"//*[contains(@class,'additional-menu')]//*[.='{buttonName}']/parent::*/following-sibling::*//*[@role='button']");

        protected IWebElement Lnk_Tab(string name)
            => FindElementByXpath($"//*[@class='new-jl-toolbar']//*[normalize-space(.)='{name}']//ancestor::a");

        [FindsBy(How = How.XPath, Using = "//*[contains(@name,'Address1') or contains(@id,'Address1')]")]
        protected IWebElement Txt_Address1 { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@name,'Address2') or contains(@id,'Address2')]")]
        protected IWebElement Txt_Address2 { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@name,'Address3') or contains(@id,'Address3')]")]
        protected IWebElement Txt_Address3 { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@name,'Address4') or contains(@id,'Address4')]")]
        protected IWebElement Txt_Address4 { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@name,'Postcode') or contains(@id,'PostCode')]")]
        protected IWebElement Txt_PostCode { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@name,'Telephone') or contains(@id,'Telephone')]")]
        protected IWebElement Txt_Telephone { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'card-title jc-center')]")]
        protected IWebElement Lbl_ModalInfo { get; set; }

        protected IWebElement Lnk_DeleteAttachment(string fileName)
            => FindElementByXpath($"//div[@id='edit_files_zone']//div[@class='row'][descendant::*[contains(.,'{fileName}')]]//a[.='Delete']");

        [FindsBy(How = How.XPath, Using = "//input[@data-vv-name='Email' or @name='Email' or @id='EmailAddress']")]
        protected IWebElement Txt_EmailAddress { get; set; }

        [FindsBy(How = How.Name, Using = "FirstName")]
        protected IWebElement Txt_FirstName { get; set; }

        [FindsBy(How = How.Name, Using = "LastName")]
        protected IWebElement Txt_LastName { get; set; }

        [FindsBy(How = How.Name, Using = "Position")]
        protected IWebElement Txt_Position { get; set; }

        [FindsBy(How = How.Name, Using = "Subject")]
        protected IWebElement Txt_Subject { get; set; }

        //[FindsBy(How = How.Name, Using = "BodyText")]
        [FindsBy(How = How.XPath, Using = "//*[@name='BodyText']//*[@role='textbox']")]
        [FindsBy(How = How.XPath, Using = "//*[@role='presentation']//*[@role='textbox']")]
        protected IWebElement Txt_Body { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'import-csv')]")]
        protected IWebElement Btn_Import { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='modalSwitchContainer']//*[@id='modalConfirmMessage']")]
        [FindsBy(How = How.XPath, Using = "//*[@id='modalSwitchContainer']")]
        protected IWebElement Lbl_ModalContent { get; set; }

        [FindsBy(How = How.Name, Using = "Notes")]
        protected IWebElement Txt_Notes { get; set; }

        [FindsBy(How = How.Name, Using = "NominalCodeId_input")]
        [FindsBy(How = How.XPath, Using = "//*[@id='po-nominal-code']//input")]
        protected IWebElement Txt_NominalCode { get; set; }

        [FindsBy(How = How.Id, Using = "logjob_Description")]  //for new Log Job page
        [FindsBy(How = How.Name, Using = "Description")]
        [FindsBy(How = How.Name, Using = "Description-0")] //for SOR job cost, quote price
        protected IList<IWebElement> Txt_Descriptions { get; set; }

        #region Search
        //Note is using SearchText, PO is using searchTerm, the others are using SearchTerm
        [FindsBy(How = How.XPath, Using = "//*[@class='relative-search']//input[contains(@name,'Search') or contains(@name,'Term')]")]
        //for some new tables
        [FindsBy(How = How.XPath, Using = "//label[contains(.,'Search')]/parent::*/input")]
        protected IList<IWebElement> Txt_SearchTerms { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'exclude-tag')]//input[contains(@class,'multiselect__input')]")]
        protected IWebElement Txt_SearchWithoutTag { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'exclude-tag')]//ul[@class='multiselect__content']")]
        protected IWebElement Lbl_SearchWithoutTagItem { get; set; }
        #endregion

        [FindsBy(How = How.Name, Using = "IncludeInactive")]
        protected IWebElement Chk_IncludeInactive { get; set; }

        //Use common Delete button for all places and asset detail, payband time modal
        [FindsBy(How = How.XPath, Using = "//*[@id='deleteButton' or @id='assetDelete' or @id='btn-deletePayBandTime']")]
        protected IWebElement Btn_Delete { get; set; }

        [FindsBy(How = How.XPath, Using = "//label[.//*[text()='Tax Rate']]/parent::*//input[@type='search']")]  //for Stock Record Detail page
        [FindsBy(How = How.Name, Using = "TaxCodeId_input")]
        [FindsBy(How = How.XPath, Using = "//*[@id='po-tax-rate']//input")]
        protected IWebElement Txt_TaxRate { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'label-danger')]")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'alert-danger')]")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'alert-info')]")]
        protected IWebElement Lbl_AlertLabel { get; set; }

        [FindsBy(How = How.Name, Using = "ShowWarning1")]
        protected IWebElement Chk_Warning1 { get; set; }

        [FindsBy(How = How.Name, Using = "ShowWarning2")]
        protected IWebElement Chk_Warning2 { get; set; }

        [FindsBy(How = How.Name, Using = "ShowWarning3")]
        protected IWebElement Chk_Warning3 { get; set; }

        [FindsBy(How = How.Name, Using = "Warning1")]
        protected IWebElement Txt_Warning1 { get; set; }

        [FindsBy(How = How.Name, Using = "Warning2")]
        protected IWebElement Txt_Warning2 { get; set; }

        [FindsBy(How = How.Name, Using = "Warning3")]
        protected IWebElement Txt_Warning3 { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(@id,'_Submit') or contains(@id,'Search')]")]
        [FindsBy(How = How.XPath, Using = "//*[text()='Search']/parent::button[@type='submit']")]
        [FindsBy(How = How.XPath, Using = "//*[text()='Search']/parent::button")] //Search button in Servive Type screen
        protected IList<IWebElement> Btn_Searchs { get; set; }

        protected IWebElement Btn_OrderBy(string columnName)
            => FindElementByXpath($"//*[text()='{columnName}']/parent::button[@class='sort-button']");

        [FindsBy(How = How.XPath, Using = "//*[@id='TagsJob_Id']//input[contains(@class, 'multiselect__input')]")]  //for new Log Job page
        [FindsBy(How = How.XPath, Using = "//input[contains(@aria-describedby,'_Tags_taglist')]")]
        [FindsBy(How = How.XPath, Using = "//input[contains(@aria-describedby,'TagIds_taglist')]")]
        protected IWebElement Txt_WithTag { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[contains(@aria-describedby,'statuses_taglist')]")]
        protected IWebElement Txt_Status { get; set; }

        [FindsBy(How = How.XPath, Using = "(//*[contains(@class,'jlAdvanceSearchTrigger')])[last()]")]
        protected IWebElement Btn_ToggleAdvance { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(.,'Reset Filter')]")]
        protected IWebElement Btn_Reset { get; set; }

        //Use list because some pages like JobType, there's alot of invisible of this element
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal') and contains(@class,'fade in')]//*[contains(@class,'jl-table-no-data') or @id='supplierBranchSearch-results']")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-table-no-data') or @id='supplierBranchSearch-results']")]
        protected IList<IWebElement> Lbl_TableNodata { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='grid-container']//*[@class='grid-content']")]
        protected IWebElement Lnk_GridItems { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'table-actions_trigger')]")]
        protected IList<IWebElement> Btn_ActionOnRows { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-table')]//tbody//tr")]
        protected IList<IWebElement> Lbl_TableRows { get; set; }

        [FindsBy(How = How.XPath, Using = "(//*[contains(@class,'jl-table')]//tbody//tr[1])[last()]")]
        protected IWebElement Lbl_TableFirstRow { get; set; }

        [FindsBy(How = How.XPath, Using = "(//*[contains(@class,'tr-group tr-group--body')]/*[contains(@class,'tr table-row extra-row')])[last()]")]
        protected IWebElement Lbl_TableFirstExtraRow { get; set; }

        [FindsBy(How = How.TagName, Using = "table")]
        protected IList<IWebElement> Tbl_GeneralTable { get; set; }

        protected IWebElement Lbl_TableItemCount(int tabIndex)
        {
            return _driver.FindElement(By.XPath($"//*[@role='tablist']//li[{tabIndex}]//*[@class='item-count']"));
        }

        [FindsBy(How = How.XPath, Using = "//*[@class='refcom-logbook-item']")]
        protected IWebElement Lbl_RefcomRows { get; set; }

        //Use for both list and grid view
        [FindsBy(How = How.XPath, Using = "//*[@role='tabpanel'][contains(@class,'active')]//*[contains(@class,'jl-table')]//td[1]//a")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-table-wrapper')]//a | //*[contains(@class,'grid-content')]/h4")]
        protected IWebElement Lnk_ViewDetails { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'modal')][@style]//*[@id='btnSave']")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'modal')][@style]//*[@id='modalConfirmYes']")]
        [FindsBy(How = How.Id, Using = "DeleteStockTake")]
        protected IWebElement Btn_Confirm { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'modal')][@style]//*[contains(@class,'jl-button-cancel')]")]
        protected IWebElement Btn_CancelConfirmation { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'Popup__WisepopPopinStyle')][@style='opacity: 1;']//*[contains(@class,'PopupCloseButton__InnerPopupCloseButton')]")]
        protected IList<IWebElement> Btn_ClosePopupNotification { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-action-buttons show')]//*[contains(@class,'jl-icon-orange')]")]
        protected virtual IWebElement Btn_EditOnRow { get; set; }

        //[FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-action-buttons show')]//*[contains(@class,'jl-icon-red')]")]
        protected IWebElement Btn_DeleteOnRow// { get; set; }
            => FindElementByXpath("//*[contains(@class,'jl-action-buttons show')]//*[contains(@class,'jl-icon-red')]");

        //Delete (trash) icon on new/recent features/tables
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-table-sticky sticky-right table-actions')]//*[contains(@class,'jl-icon-red')]")]
        protected IList<IWebElement> Btn_DeleteIconOnRow { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='modal fade in']//*[contains(@class,'jl-button-save')]//*[contains(@class,'jli jl-content-save')]")]
        protected IWebElement Btn_ConfirmSave { get; set; }

        protected IWebElement Lnk_Cell(int colunn, int? row = null)
            => FindElementByXpath($"//*[contains(@class,'jl-table')]//tbody//tr[{(row != null ? row : 1)}]//td[{colunn}]/a");

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal fade in')]//*[contains(@class,'content-save')]/parent::*")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'content-save')]/parent::*")]
        protected IList<IWebElement> Btn_Saves { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@data-dismiss='modal']")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'close-circle')]/parent::*")]
        protected IList<IWebElement> Btn_Cancels { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal fade in')]//*[.='OK']/parent::*")]
        protected IWebElement Btn_Ok { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@style,'display: block')]//*[.='Yes']/parent::button")]
        protected IWebElement Btn_YesOnModal { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@style,'display: block')]//*[.='No']/parent::button")]
        protected IWebElement Btn_NoOnModal { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'emai')]//*[@class='modal-footer']//button//*[text()='Send']")]
        protected IWebElement Btn_Send { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'k-nodata')]")]
        protected IList<IWebElement> Lbl_Nodatalist { get; set; }

        protected IWebElement Btn_AddOTF(string objectName, string attribute = "name")
        {
            string xpath;
            try
            {
                xpath = $"//*[contains(@class,'jl-modal-add')][contains(@onclick,'{objectName}')]";
                return FindElementByXpath(xpath);
            }
            catch (Exception ex) when (
                ex is NoSuchElementException
                || ex is WebDriverTimeoutException
            )
            {
                //xpath = $"//*[contains(@{attribute},'{objectName}')]/following-sibling::*//button[@class='jl-modal-add']";
                xpath = $"//*[@{attribute}='{objectName}']/following-sibling::*//button[@class='jl-modal-add']";
                return FindElementByXpath(xpath);
            }
        }

        //For VueJs controls that doesn't have onclick attribute

        protected IWebElement Btn_AddOTFVue(string objectName)
            => FindElementByXpath($"//label[normalize-space()='{objectName}']/following-sibling::*//*[contains(@class,'jl-modal-add')]");

        [FindsBy(How = How.XPath, Using = "//*[@class='snackbar open']//*[contains(@class,'jlSaveEditAble')]")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'steps-footer')]//*[contains(@class,'jl-content-save')]/parent::*")]
        protected IWebElement Btn_SaveOnEdit { get; set; }

        //Edit button for both big and small resolution
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'button-orange')][contains(@id,'edit')]")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'button-orange')]//*[contains(text(),'Edit')]")]
        protected IList<IWebElement> Btn_Edits { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='snackbar open']//*[contains(@class,'jlMakeEditAble-Undo')]")]
        protected IWebElement Btn_Undo { get; set; }

        [FindsBy(How = How.Id, Using = "LJPriorityDescription")]
        [FindsBy(How = How.Name, Using = "PriorityId_input")]
        protected IWebElement Txt_Priority { get; set; }

        //New Priority by vuejs
        [FindsBy(How = How.XPath, Using = "//*[@id='PriorityLevelJob_Id']//input")]  //for new Log Job page
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'v-select')][@name='Description']//input")]
        protected IWebElement Txt_VuePriority { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='PrimaryTradeJob_Id']//input")]  //for new Log Job page
        [FindsBy(How = How.XPath, Using = "//*[@name='TradeId_input' or @aria-describedby='TradeIds_taglist']")]
        protected IWebElement Txt_Trade { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@name='AreaId_input' or @aria-describedby='AreasId_taglist']")]
        protected IWebElement Txt_Area { get; set; }

        [FindsBy(How = How.Name, Using = "SellingRateId")]
        [FindsBy(How = How.Name, Using = "PPMSellingRateId_input")]
        [FindsBy(How = How.XPath, Using = "//*[text()='Selling Rate ']/parent::*//input[@type='search']")]
        protected IWebElement Txt_SellingRate { get; set; }

        [FindsBy(How = How.Id, Using = "UberLink")]
        protected IWebElement Txt_PortalLink { get; set; }

        protected IWebElement Btn_SortColumn(string columnName)
            => FindElementByXpath($"//button[@class='sort-button']/span[text()='{columnName}']");

        //View as 'list' or 'grid'
        protected IWebElement Btn_ViewAs(string mode)
            => FindElementByXpath($"//*[@type='button'][contains(@class,'{mode}')]");

        protected IWebElement Lnk_TabSearchPage(string tabName)
            => FindElementByXpath($"//*[@role='tablist']//*[text()='{tabName}']");
        protected IWebElement Lnk_TabSearchPage(int index)
            => FindElementByXpath($"//*[@role='tablist']/li[{index}]");

        [FindsBy(How = How.XPath, Using = "//*[@id='downloadDocument']/parent::*/following-sibling::a[contains(@onclick,'DocumentTemplates')]")]
        protected IWebElement Btn_DownloadOptions { get; set; }

        protected IWebElement Btn_Download(string templateName)
            => FindElementByXpath($"//a[contains(@data-url,'/Download')]//*[text()='{templateName}']");

        [FindsBy(How = How.XPath, Using = "//*[contains(@onclick,'/Email/')]/parent::*/following-sibling::a[contains(@onclick,'DocumentTemplate')]")]
        protected IWebElement Btn_EmailOptions { get; set; }

        [FindsBy(How = How.Id, Using = "emailButton")]
        protected IWebElement Btn_Email { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'template-containers')][@style='display: block;']/li")]
        protected IList<IWebElement> Btn_Emails { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@name,'__privateStripeFrame')]")]
        protected IWebElement Frm_CardInfo { get; set; }

        [FindsBy(How = How.Name, Using = "cardnumber")]
        protected IWebElement Txt_Number { get; set; }

        [FindsBy(How = How.Name, Using = "cvc")]
        protected IWebElement Txt_CVC { get; set; }

        [FindsBy(How = How.Name, Using = "postal")]
        protected IWebElement Txt_Zip { get; set; }

        [FindsBy(How = How.Name, Using = "exp-date")]
        protected IWebElement Txt_Expiry { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='modal fade in']//*[@class='modal-content']")]
        [FindsBy(How = How.XPath, Using = "//*[@class='modal-content']")]
        protected IWebElement Lbl_ActiveModal { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='modal fade in']//*[@class='modal-content']")]
        [FindsBy(How = How.XPath, Using = "//*[@class='modal-content']")]
        protected IList<IWebElement> Lbl_ActiveModals { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal')][contains(@class,'fade in')]//*[@class='modal-body']")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'modal')][contains(@class,'fade in')]")]
        protected IWebElement Lbl_ActiveModalMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='modal-content']//input[@id='agreeCheckbox']")]
        [FindsBy(How = How.XPath, Using = "//*[@class='modal-content']//input[@id='deleteConfirmation']/following-sibling::*")]
        protected IList<IWebElement> Chk_Delete { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='TypeJob_Id']//input")]  //for new Log Job page
        [FindsBy(How = How.Name, Using = "JobTypeId_input")]
        protected IWebElement Txt_JobType { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='CategoryJob_Id']//input")]  //for new Log Job page
        [FindsBy(How = How.XPath, Using = "//*[@id='ppmVisitdetails'][contains(@class,'active')]//*[@name='JobCategoryId_input']")] //for PPM Visit details, active visit card
        [FindsBy(How = How.Name, Using = "JobCategoryId_input")]
        [FindsBy(How = How.XPath, Using = "//*[text()='Job Category']/parent::*//input[@type='search']")]
        protected IWebElement Txt_JobCategory { get; set; }

        [FindsBy(How = How.Name, Using = "ClassId_input")]
        [FindsBy(How = How.Name, Using = "EquipmentClass")]
        protected IWebElement Txt_AssetClass { get; set; }

        [FindsBy(How = How.XPath, Using = "(//div[contains(@class,'jl-assets-list')])[1]//*[contains(@class,'jl-card-box-wrapper ltr')]")]
        protected IList<IWebElement> Lbl_SiteAssets { get; set; }

        [FindsBy(How = How.XPath, Using = "(//div[contains(@class,'jl-assets-list')])[1]//*[contains(@class,'jl-card-box-wrapper ltr')]//*[contains(@class, 'jl-pencil-circle')]")]
        protected IList<IWebElement> Btn_EditSiteAsset { get; set; }

        [FindsBy(How = How.XPath, Using = "(//div[contains(@class,'jl-assets-list')])[2]//*[contains(@class,'jl-card-box-wrapper ltr')]")]
        protected IList<IWebElement> Lbl_MovedAssets { get; set; }

        [FindsBy(How = How.XPath, Using = "(//div[contains(@class,'jl-assets-list')])[2]//*[contains(@class,'jl-card-box-wrapper ltr')]//*[contains(@class, 'jl-pencil-circle')]")]
        protected IList<IWebElement> Btn_EditMovedAsset { get; set; }

        [FindsBy(How = How.Name, Using = "JobAssetServiceTypeId_input")]
        protected IWebElement Txt_ServiceType { get; set; }

        [FindsBy(How = How.Id, Using = "jobAssetDuration")]
        protected IWebElement Txt_Minutes { get; set; }

        [FindsBy(How = How.Id, Using = "CustomReference")]
        protected IWebElement Txt_ReferenceNumber { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@id,'-error') or contains(@id,'_validationMessage')  or @class='error']")]
        protected IList<IWebElement> Lbl_Errors { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@name='jl-multiselect-selectedengineer']/preceding-sibling::input[contains(@style,'display')]")]
        protected IWebElement Cbb_RecurEngineers { get; set; }

        #region Css element
        [FindsBy(How = How.XPath, Using = "//*[@role='tabpanel'][contains(@class,'active')]//*[contains(@class,'jl-search-box')]//label[not(contains(@class,'danger')) and not(contains(@class,'bullet'))]")]
        [FindsBy(How = How.XPath, Using = "//*[@role='tabpanel'][contains(@class,'active')]//label[not(contains(@class,'danger')) and not(contains(@class,'bullet'))]")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-search-box')]//label[not(contains(@class,'danger')) and not(contains(@class,'bullet'))]")]
        [FindsBy(How = How.XPath, Using = "//form[not(ancestor::*[@role='tabpanel']) and not(contains(@action,'Search'))]//label[not(contains(@class,'danger')) and not(contains(@class,'bullet'))]")]
        protected IList<IWebElement> Lbl_Labels { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@role='tabpanel'][contains(@class,'active')]//input[@type='text' or @type='number'][not(@disabled)][not(@role)]")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-search-box')]//input[@type='text' or @type='number'][not(@disabled)][not(@role)]")]
        [FindsBy(How = How.XPath, Using = "//form[not(ancestor::*[@role='tabpanel']) and not(contains(@action,'Search'))]//input[@type='text' or @type='number'][not(@disabled)][not(@role)]")]
        [FindsBy(How = How.XPath, Using = "//textarea[not(@disabled)]")]
        protected IList<IWebElement> Txt_EnabledTextBoxes { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@role='tabpanel'][contains(@class,'active')]//input[contains(@class,'k-input form-control')]")]
        [FindsBy(How = How.XPath, Using = "//input[contains(@class,'k-input form-control')]")]
        protected IList<IWebElement> Txt_EnabledCustomTextBoxes { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@role='tabpanel'][contains(@class,'active')]//input[starts-with(@class,'form-control')][@disabled][not(@placeholder)]")]
        protected IList<IWebElement> Txt_DisabledTextBoxes { get; set; }

        //[FindsBy(How = How.XPath, Using = "//*[@role='tabpanel'][contains(@class,'active')]//input[@disabled][contains(@class,'k-input form-control')]")]
        [FindsBy(How = How.CssSelector, Using = ".active[role='tabpanel'] input.k-input.form-control[disabled]")]
        protected IList<IWebElement> Txt_DisabledCustomTextBoxes { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@role='tabpanel'][contains(@class,'active')]//button[contains(@class,'jl-modal-add')][@disabled]")]
        protected IList<IWebElement> Btn_DisabledPlusIcons { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'jl-modal-add')]")]
        protected IList<IWebElement> Btn_EnabledPlusIcons { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-search-box')]//select[not(@multiple)]")]
        protected IList<IWebElement> Cbb_SingleSelect { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-search-box')]//select[@multiple]/parent::*")]
        protected IList<IWebElement> Cbb_MultiSelect { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-content-wrap')][contains(@class,'loading')]")]
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'customize-jl-loading')][contains(@class,'active')]")]
        protected IList<IWebElement> Lbl_Loadings { get; set; }
        #endregion

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-star')]")]
        protected IWebElement Btn_MakeDefault { get; set; }

        [FindsBy(How = How.XPath, Using = "//label[normalize-space()='Default']")]
        protected IWebElement Lbl_Default { get; set; }

        [FindsBy(How = How.ClassName, Using = "jl-pagination-wrap")]
        protected IWebElement Lbl_Pagenation { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='label label-danger']")]
        protected IWebElement Lbl_Suspended { get; set; }
        protected IWebElement Txt_StartDate(string name)
            => FindElementByXpath($"//label[normalize-space()='{name}']/parent::*//*[contains(translate(@id,'SD','sd'),'startdate')]");

        protected IWebElement Txt_EndDate(string name, bool isPPM = false)
        {
            if (!isPPM)
                return FindElementByXpath($"//label[normalize-space()='{name}']/parent::*/following-sibling::*//*[contains(translate(@id,'ED','ed'),'enddate')]");
            else
                return FindElementByXpath($"//label[normalize-space()='{name}']/parent::*//*[contains(translate(@id,'ED','ed'),'enddate')]");
        }

        [FindsBy(How = How.XPath, Using = "//*[@id='deselectPrimaryContact']")]
        protected IWebElement Btn_SetPrimaryContact { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@id, 'contactModule')]//*[contains(@class, 'table-no-data')]")]
        protected IWebElement Lbl_ContactSectionTableNoData { get; set; }

        #region Quick Filter
        [FindsBy(How = How.XPath, Using = "//*[@class='quick-filter-tab']")]
        protected IWebElement Btn_QuickFilters { get; set; }

        protected IWebElement Lnk_QuickFilter(string name)
        {
            //string xpath = $"//*[@class='quick-filter']//*[@class='content']//*[@class='name'][contains(.,'{name}')]";
            string xpath = $"//*[@id='quick-filter']//*[@class='content']//*[@class='name'][contains(.,'{name}')]";
            return FindElementByXpath(xpath);
        }

        protected IWebElement Btn_DeleteQuickFilter(string name)
        {
            string xpath = $"//*[@id='quick-filter']//*[@class='content']//*[@class='name'][contains(.,'{name}')]/parent::*//following-sibling::*//*[contains(@class, 'action-buttons')]/span";
            return FindElementByXpath(xpath);
        }

        [FindsBy(How = How.XPath, Using = "//*[@class='quick-filter']//*[@class='content']//p")]
        protected IWebElement Lbl_QuickFilterMessage { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='save-filter']")]
        protected IWebElement Btn_SaveFilter { get; set; }

        protected IWebElement Icon_Private(string name)
        {
            string xpath = $"//*[@class='quick-filter']//*[@class='content']//*[@class='name'][contains(.,'{name}')]/preceding-sibling::*//span";
            return FindElementByXpath(xpath);
        }

        protected IWebElement Lbl_QuickFilterCount(string name)
        {
            string xpath = $"//*[@class='quick-filter']//*[@class='content']//*[@class='name'][contains(.,'{name}')]/parent::*/following-sibling::*/*[@class='count']";
            return FindElementByXpath(xpath);
        }

        [FindsBy(How = How.XPath, Using = "//input[@name='Filters']")]
        protected IList<IWebElement> Rdo_SaveQickFilterOptions { get; set; }
        #endregion

        #region Suspend PPM
        [FindsBy(How = How.XPath, Using = "//*[@class='row suspended-ppm']")]
        protected IWebElement Lbl_PPMSuspend { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='suspended-link']")]
        protected IWebElement Lnk_Here { get; set; }
        #endregion
        #endregion

        public virtual void Open()
        {
            var _url = Url;

            if (Url == null)
                _url = BaseUrl;
            else
            {
                if (!Url.Contains("http") || !Url.Contains("https"))
                {
                    _url = BaseUrl + _url;
                }
            }


            //App.Navigation.Navigate(new Uri(baseUrl));
        }
    }
}