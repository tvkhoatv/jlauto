﻿using Infrastructure.Models;
using JLAuto.Factory;
using JLAuto.Infrastructure.Base;
using JLAuto.Infrastructure.Models;
using JLAuto.Utils;
using OpenQA.Selenium;
using System.Diagnostics;
using static MongoDB.Driver.WriteConcern;
using FluentAssertions;
using System.Threading.Tasks;
using System;

namespace JLAuto.Base
{
    public abstract partial class BasePage<P, M>
        where P : BasePage<P, M>
        where M : IModel
    {

        #region Common save
        public P Save(M model, bool isOtf = false, bool isSave = true)
        {
            _isOtf = isOtf; //use for adding ppm contract
            _model = model; //Use for add price/cost
                            //try
                            //{
            _testUtil?.LogInfo($"Starting to save {model.GetType().Name}");

            EnableEditable();

            if (isOtf)
            {
                CreatOtf(model);
            }
            else
            {
                Select(model);
            }
            CommonSave(model, isSave);
            waitHelper.WaitForPageFullLoaded();
            //}
            //catch (Exception ex)
            //{
            //_testUtil?.LogFail(ex);

            //throw ex;
            //}
            return (P)this;
        }

        public virtual void EnableEditable() { }
        protected virtual void CommonSave(M model, bool isSave = true) { }
        protected virtual void CreatOtf(M model) { }
        protected virtual void Select(M model) { }
        #endregion

        #region BaseAction
        protected void TakeActions(Action action)
        {
            try
            {
                string methodName = new StackTrace().GetFrame(1).GetMethod().Name;
                _testUtil?.LogInfo($"Starting {methodName.SplitByUperCase()}");

                //    if (Btn_ClosePopupNotification != null && Btn_ClosePopupNotification.Any())
                //    {
                //        Btn_ClosePopupNotification.ToList().ForEach(x => keywords.ClickOn(x));
                //    }

                action();
            }
            catch (Exception ex)
            {
                _testUtil?.LogFail(_driverFactory, ex);

                //    ServiceWebPageFactory.Queue.Enqueue(_driverFactory);
                //    //throw ex;
                throw new ArgumentException(_driverFactory.GetCurrentURL(), ex);
            }
        }
        #endregion

        #region Base Verification
        protected void Verification(Action action, bool needEnqueue = true)
        {
            try
            {
                action();
                string methodName = new StackTrace().GetFrame(1).GetMethod().Name;
                _testUtil?.LogPass(_driverFactory, $"{methodName.SplitByUperCase()}");
            }
            catch (Exception ex)
            {
                _testUtil?.LogFail(_driverFactory, ex);

                //if (needEnqueue)
                //    ServiceWebPageFactory.Queue.Enqueue(_driverFactory);

                throw ex;
            }
        }

        protected async Task VerificationAsync(Func<Task> action)
        {
            try
            {
                await action();
                string methodName = new StackTrace().GetFrame(1).GetMethod().Name;
                _testUtil?.LogPass(methodName.SplitByUperCase());
            }
            catch (Exception ex)
            {
                _testUtil?.LogFail(ex);

                //ServiceWebPageFactory.Queue.Enqueue(_driverFactory);
                throw ex;
            }
        }
        #endregion
    }
}
