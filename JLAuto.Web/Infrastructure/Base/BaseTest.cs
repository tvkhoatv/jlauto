﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JLAuto.DI;
using JLAuto.Factory;
using Infrastructure.Utils;
using Bogus.DataSets;
using System.Diagnostics;
using libphonenumber;
using JLAuto.Utils;
using DocumentFormat.OpenXml.Vml;

namespace JLAuto.Web.Infrastructure.Base
{
    public abstract partial class BaseTest
    {
        public DriverFactory? driverFactory;
        protected TestUtil? testUtil;
#pragma warning disable CS8618
        public TestContext TestContext { get; set; }
#pragma warning disable CS8618

        [TestInitialize]
        public void CoreTestInit()
        {
        }
    }
}
