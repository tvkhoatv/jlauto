﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using TestContextNunit = NUnit.Framework.TestContext;

namespace JLAuto.Web
{
    [TestFixture]
    [NonParallelizable]
    public abstract class BaseTestNunit
    {
        protected IWebDriver? driver;
        TestContextNunit Context;

        protected BaseTestNunit() {
            
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var status = TestContextNunit.CurrentContext.Result.Outcome.Status;
            //UiTestSession.Current.Start(context);
        }

        [SetUp]
        public void SetUp()
        {

        }

        [TearDown]
        public void TearDown()
        {
            Context = TestContextNunit.CurrentContext;
            var status = Context.Result.Outcome.Status;
            string testClassName = Context.Test.Name;
            string testMethodName = Context.Test.MethodName;
            var stacktrace = string.IsNullOrEmpty(Context.Result.StackTrace) ? "" : string.Format("{0}", Context.Result.StackTrace);

            switch (status)
            {
                case TestStatus.Failed:
                    break;

                case TestStatus.Skipped:
                    break;

                default:
                    Console.WriteLine("Test status is Pass");
                    break;
            }
        }


        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            //UiTestSession.Current.CleanUp();
        }

        public void ShowThreadId()
        {
            Console.WriteLine(AppDomain.GetCurrentThreadId());
        }
    }


}
