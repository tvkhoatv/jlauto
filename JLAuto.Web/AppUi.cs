﻿using JLAuto.DI;
using JLAuto.Settings;
using JLAuto.Enums;
using JLAuto.Utils;
using JLAuto.Infrastructure.Variables;
using JLAuto.Infrastructure.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Options;
using JLAuto.Base;
using JLAuto.Factory;
using DriverFactory = JLAuto.Factory.DriverFactory;
using BaseApp = JLAuto.Infrastructure.BaseApp;
using System.Linq;
using System;

namespace JLAuto.Web
{
    public class AppUi : BaseApp
    {
        public ServicesCollection Container;

        public static AppUi Instance => new Lazy<AppUi>(() => new AppUi()).Value;

        private AppUi() : base()
        {
        }

        public override void Initialize(TestContext context)
        {
            this.Configure(new RuntimeSetting(context));
            ServiceRegistry.Register();
        }

        public override void CleanUp()
        {
            DriverFactory driverFactory = ServicesCollection.Current.Resolve<DriverFactory>();
            driverFactory.Dispose();
        }

        private void Configure(RuntimeSetting setting)
        {
            var browserTypes = setting.GetProperty("BrowserType").ToString().Split(',').Select(x => EnumHelper.GetEnumByValue<BrowserType>(x));

            AppConfigFactory.Environment = setting.GetProperty("Environment").ToString();
            AppConfigFactory.Region = setting.GetProperty("Region").ToString();
            AppConfigFactory.Project = "JL Web";
            AppConfigFactory.TestCategory = setting.GetProperty("TestCategory").ToString();
            AppConfigFactory.BrowserTypes = browserTypes;
            AppConfigFactory.Device = EnumHelper.GetEnumByValue<Device>(setting.GetProperty("WebDevice").ToString());
            AppConfigFactory.TenantType = setting.GetProperty("TenantType").ToString();

            //System.Environment.SetEnvironmentVariable("BrowserType", AppConfigFactory.BrowserTypes.FirstOrDefault().ToString());

            Container = ServicesCollection.Current.CreateChildServicesCollection(setting.GetContext().FullyQualifiedTestClassName);
            Container.RegisterInstance(Container);
            ServicesCollection.Current.RegisterInstance<DriverFactory>(new DriverFactory(
                new DriverOptions()
                {
                    BrowserType = AppConfigFactory.BrowserTypes.FirstOrDefault()
                }
            ));
        }

        public TPage LoadPage<TPage>()
            where TPage : BasePage<TPage, IModel>
        {
            if (ServicesCollection.Current.IsRegistered<TPage>() == false)
                ServicesCollection.Current.RegisterType<TPage>(true);

            TPage page = ServicesCollection.Current.Resolve<TPage>();
            return page;
        }

        public TPage GoTo<TPage>()
            where TPage : BasePage<TPage, IModel>
        {
            if (ServicesCollection.Current.IsRegistered<TPage>() == false)
                ServicesCollection.Current.RegisterType<TPage>(true);

            TPage page = ServicesCollection.Current.Resolve<TPage>();
            page.Open();
            return page;
        }
    }
}
