﻿using FluentAssertions;
using JLAuto.Infrastructure.Models;
using JLAuto.Base;
using JLAuto.Factory;
using JLAuto.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace JLAuto.Web.PageObjects
{
    public abstract class Login<P, M> : BasePage<P, M>
        where P : Login<P, M>
        where M : IModel
    {
        #region Constructor
        protected Login() : base() { }
        protected Login(DriverFactory driverFactory) : base(driverFactory) { }
        protected Login(DriverFactory driverFactory, TestUtil testUtil) : base(driverFactory, testUtil) { }
        #endregion

        #region Declare elements

        [FindsBy(How = How.Id, Using = "accountMenu")]
        protected IWebElement Lbl_AccountMenu { get; set; }

        [FindsBy(How = How.Id, Using = "UserName")]
        protected IWebElement Txt_username { get; set; }

        [FindsBy(How = How.Id, Using = "Password")]
        protected IWebElement Txt_password { get; set; }

        [FindsBy(How = How.Id, Using = "loginButton")]
        protected IWebElement Btn_login { get; set; }

        [FindsBy(How = How.Id, Using = "signUp")]
        protected IWebElement Btn_CreateAnAccount { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Forgot Password')]")]
        protected IWebElement Btn_ForgotPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'dashboard-container')]")]
        protected IWebElement Lbl_DashboardContainer { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@name='UserName']/following-sibling::*[@class='error']")]
        protected IWebElement Lbl_UsernameError { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@name='Password']/following-sibling::*[@class='error']")]
        protected IWebElement Lbl_PasswordError { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'logonErrors')]")]
        protected IWebElement Lbl_LoginError { get; set; }

        [FindsBy(How = How.Id, Using = "sendInstructionButton")]
        protected IWebElement Btn_SendIntructions { get; set; }


        [FindsBy(How = How.XPath, Using = "//script[contains(text(),'window.intercomSettings')]")]
        protected IWebElement Lbl_Intercom { get; set; }

        [FindsBy(How = How.Name, Using = "__RequestVerificationToken")]
        protected IWebElement Txt_RequestVerificationToken { get; set; }

        #endregion

        #region Login successfully
        /// <summary>
        /// Verify Account Profile present.
        /// </summary>
        public void VerifyAccountMenuPresent(bool fromSubDriver = false)
        {
            Verification(() =>
            {
                waitHelper.WaitForPageFullLoaded();
                waitHelper.WaitForElementEnable(Lbl_Intercom);
                waitHelper.WaitForElementVisible(Lbl_AccountMenu);

                Lbl_AccountMenu.Displayed.Should().BeTrue();

                ////Get login info
                //var cookies = _driver.Manage().Cookies.AllCookies;

                var requestToken = Txt_RequestVerificationToken.GetAttribute("value");

                //var httpRequestHeader = new HttpRequestHeaderModel(cookies, requestToken);

                ////Add region part to main url
                //if (!fromSubDriver)
                //{
                //    AppConfigFactory.Instance.Url = $"{AppConfigFactory.Instance.Url}/{httpRequestHeader.Region}";
                //    WebApi.RequestHeaders[CookieType.Web] = httpRequestHeader;
                //}
                //else
                //{
                //    WebApi.RequestHeaders[CookieType.SubWeb] = httpRequestHeader;
                //}

                //Get TenantId for Jicro
                //if (AppConfigFactory.Instance.InitBy == InitBy.Jicro)
                //{
                //    var intercomSetting = JObject.Parse(Lbl_Intercom.GetAttribute("innerText")
                //                                  .Replace("\r\n", "")
                //                                  .Replace("window.intercomSettings = ", "")
                //                                  .Replace(";", "")
                //                                  .Trim());
                //    AppConfigFactory.Instance.TenantId = Guid.Parse(intercomSetting["company"]["id"].ToString());
                //}
            });
        }
        #endregion
    }
}
