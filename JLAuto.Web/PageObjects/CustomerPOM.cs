﻿//using DocumentFormat.OpenXml.Vml;
//using Domain.Models;
using FluentAssertions;
using Infrastructure.Models;
//using Infrastructure.Base;
//using Infrastructure.Utils;
using JLAuto.Base;
using JLAuto.Factory;
using JLAuto.Utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;
using System.Linq;
using JLAuto.Infrastructure.Models;

namespace JLAuto.PageObject.JLWeb
{
    public abstract class CustomerPOM<P, M> : BasePage<P, M>
        where P : CustomerPOM<P, M>
        where M : IModel
    {
        #region Construtor
        public CustomerPOM() : base() { }
        public CustomerPOM(DriverFactory driverFactory) : base(driverFactory) { }
        public CustomerPOM(DriverFactory driverFactory, TestUtil testUtil) : base(driverFactory, testUtil) { }
        #endregion

        #region Declare elements


        //[FindsBy(How = How.Id, Using = "CustomerName")]
        protected IWebElement Btn_TaxRateOTF
        {
            get
            {
                return Btn_AddOTF("TaxRateId");
            }
        }

        [FindsBy(How = How.Id, Using = "CustomerName")]
        protected IWebElement Txt_Name { get; set; }

        [FindsBy(How = How.Name, Using = "AccountNumber")]
        protected IWebElement Txt_AccountNumber { get; set; }

        [FindsBy(How = How.Name, Using = "CustomerTypeId_input")]
        protected IWebElement Txt_CustomerType { get; set; }

        [FindsBy(How = How.Name, Using = "SellingRateId_input")]
        protected IWebElement Txt_CustomerSellingRate { get; set; }

        [FindsBy(How = How.Id, Using = "ContactFirstName")]
        protected IWebElement Txt_ContactFirstName { get; set; }

        [FindsBy(How = How.Id, Using = "ContactLastName")]
        protected IWebElement Txt_ContactLastName { get; set; }

        [FindsBy(How = How.Id, Using = "ContactTelephone")]
        protected IWebElement Txt_ContactTelephone { get; set; }

        [FindsBy(How = How.Id, Using = "ContactEmail")]
        protected IWebElement Txt_ContactEmail { get; set; }

        [FindsBy(How = How.Id, Using = "ContactPosition")]
        protected IWebElement Txt_ContactPosition { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@targettab,'contactsTab')]//button[contains(@class,'button-green')]")]
        protected IWebElement Btn_AddContact { get; set; }

        [FindsBy(How = How.Id, Using = "createSiteButtonMenu")]
        protected IWebElement Btn_AddSite { get; set; }

        [FindsBy(How = How.Id, Using = "createJobButtonMenu")]
        protected IWebElement Btn_LogJob { get; set; }

        [FindsBy(How = How.Id, Using = "createQuoteButtonMenu")]
        protected IWebElement Btn_LogQuote { get; set; }

        [FindsBy(How = How.Id, Using = "BillingName")]
        protected IWebElement Txt_BillingName { get; set; }

        [FindsBy(How = How.Id, Using = "BillingAddress1")]
        protected IWebElement Txt_BillingAddress1 { get; set; }

        [FindsBy(How = How.Id, Using = "BillingAddress2")]
        protected IWebElement Txt_BillingAddress2 { get; set; }

        [FindsBy(How = How.Id, Using = "BillingAddress3")]
        protected IWebElement Txt_BillingAddress3 { get; set; }

        [FindsBy(How = How.Id, Using = "BillingAddress4")]
        protected IWebElement Txt_BillingAddress4 { get; set; }

        [FindsBy(How = How.Id, Using = "BillingPostcode")]
        protected IWebElement Txt_BillingPostCode { get; set; }

        [FindsBy(How = How.Id, Using = "BillingTelephone")]
        protected IWebElement Txt_BillingTelephone { get; set; }

        [FindsBy(How = How.Name, Using = "UseBillingAddress")]
        protected IWebElement Chk_EnableBilling { get; set; }

        [FindsBy(How = How.Name, Using = "EmailAddress")]
        protected IWebElement Txt_BillingEmail { get; set; }

        [FindsBy(How = How.Id, Using = "BillingAccountNumber")]
        protected IWebElement Txt_BillingAccountNumber { get; set; }

        [FindsBy(How = How.Name, Using = "VatNumber")]
        protected IWebElement Txt_VatNumber { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@aria-describedby,'TagIds_taglist')]")]
        protected IWebElement Txt_CustomerTag { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id ='TagIds_listbox']//li")]
        protected IWebElement Lbl_CustomerTag { get; set; }

        [FindsBy(How = How.Name, Using = "Active")]
        protected IWebElement Chk_ActiveInDetails { get; set; }

        [FindsBy(How = How.XPath, Using = "//label[contains(normalize-space(),'Active')]/input")]
        protected IWebElement Chk_Active { get; set; }

        [FindsBy(How = How.Id, Using = "createCGroupInvoiceButtonMenu")]
        protected IWebElement Btn_AddCGroupInvoice { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='additional-menu']//a[contains(@href,'/Customer/Create')]")]
        protected IWebElement Btn_AddCustomer { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[span[.='Assign Schedule of Rates Library']]")]
        protected IWebElement Btn_AssignSORLibrary { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='jl-switch-isshowportallink']/following-sibling::*[contains(@class,'v-switch-core')]")]
        protected IWebElement Swt_ShowPortalLinkOnInvoice { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-table-sticky sticky-right table-actions')]//*[contains(@class,'jl-icon-red')]//parent::button")]
        protected IList<IWebElement> Btn_DeleteSORLibrary { get; set; }

       
        #endregion

        #region verify order by
        public void VerifyTableOrderedBy(IEnumerable<IModel> customers)
        {
            Verification(() =>
            {
                //waitHelper.WaitForElementVisible(Lbl_Message);
                //waitHelper.WaitForPageFullLoaded();
                //var table = keywords.GetRows(Lbl_TableRows, "innerText");
                ////First row is first element of sites
                //table.FirstOrDefault()
                //     .FirstOrDefault()
                //     .Should()
                //     .Contain(((CustomerModel)customers.FirstOrDefault()).Name);

                ////Last row is last element of sites
                //table.LastOrDefault()
                //     .FirstOrDefault()
                //     .Should()
                //     .Contain(((CustomerModel)customers.LastOrDefault()).Name);
            });
        }
        #endregion

        #region Get UniqueId for related site
        //public CustomerModel GetUniqueId(CustomerModel customer)
        //{
        //    waitHelper.WaitForPageFullLoaded();
        //    customer.UniqueId = Guid.Parse(Txt_EntityId.GetAttribute("value"));
        //    return customer;
        //}
        #endregion

        #region Verify Customer present
        /// <summary>
        /// Verify Customer Details present.
        /// </summary>
        public void VerifyCustomerDetailsPagePresent()
        {
            Verification(() =>
            {
                waitHelper.WaitForElementVisible(Lbl_Title);

                Lbl_Title.Displayed.Should().BeTrue();
                //Txt_Name.GetAttribute("value").Should().Be(customer.Name);

                //var address = customer.Address;
                //if (address != null)
                //{
                //    Txt_Address1.GetAttribute("value").Should().Be(address.Address1);
                //    Txt_Address2.GetAttribute("value").Should().Be(address.Address2);
                //    Txt_Address3.GetAttribute("value").Should().Be(address.Address3);
                //    Txt_Address4.GetAttribute("value").Should().Be(address.Address4);
                //    Txt_PostCode.GetAttribute("value").Should().Be(address.PostCode);
                //    //Txt_Telephone.GetAttribute("value")
                //    //             .Replace(" ", string.Empty)
                //    //             .Should()
                //    //             .Contain(address.Telephone);
                //}
                //Txt_CustomerType.GetAttribute("value").Should().Be(customer.CustomerType?.Description ?? string.Empty);
                //if (customer.Tags != null)
                //{
                //    var actual = Txt_CustomerTag.FindElement(By.XPath("parent::*")).GetAttribute("innerText");
                //    foreach (var tag in customer.Tags)
                //    {
                //        actual.Should().Contain(tag.Title);
                //    }
                //}
                //Chk_Active.Selected.Should().Be(customer.IsActive);
            });
        }
        #endregion
    }
}
