﻿using JLAuto.Factory;
using Infrastructure.Models;
using JLAuto.Utils;
using JLAuto.Web.PageObjects;
using JLAuto.Infrastructure.Models;

namespace JLAuto.Web.Business
{
    public class LoginPage : Login<LoginPage, IModel>
    {
        #region Constructor
        private LoginPage() : base() { }
        public LoginPage(DriverFactory driverFactory) : base(driverFactory) { }
        public LoginPage(DriverFactory driverFactory, TestUtil testUtil) : base(driverFactory, testUtil) { }
        #endregion

        #region Sign on
        public LoginPage SignOn(Credential credential)
        {
            TakeActions(() =>
            {
                keywords.SendkeysOnTextBox(Txt_username, credential?.EmailAddress);
                keywords.SendkeysOnTextBox(Txt_password, credential?.Password);
                keywords.JSClickOn(Btn_login);
            });
            return this;
        }
        #endregion

        #region click on Sign up
        public LoginPage ClickOnSignUp()
        {
            TakeActions(() =>
            {
                keywords.ClickOn(Btn_CreateAnAccount);
            });
            return this;
        }
        #endregion

        #region Click on Forgot Password
        public LoginPage ForgotPassword(string emailInstruction)
        {
            //TakeActions(async () =>
            //{
            //    waitHelper.WaitForPageFullLoaded();

            //    keywords.ClickOn(Btn_ForgotPassword);
            //    keywords.SendkeysOnTextBox(Txt_EmailAddress, emailInstruction);
            //    keywords.ClickOn(Btn_SendIntructions);

            //    if (Regex.IsMatch(emailInstruction, "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"))
            //    {
            //        var email = await GMail.Instance
            //                         .GetMail("Password Reset Request", emailInstruction);
            //        email.Should().NotBeNull();

            //        ////Get activation url
            //        var startIndex = email.Body.IndexOf("Reset My Password ( ") + 20;
            //        var endIndex = email.Body.IndexOf(" )Did You");
            //        var activateUrl = email.Body.Substring(startIndex, endIndex - startIndex);

            //        //Activate user from received email
            //        _driverFactory.NavigateToUrl(activateUrl);
            //    }
            //});
            return this;
        }

        #endregion
    }
}
