﻿//using Domain.PageObject.JLWeb;
//using Infrastructure.Utils;
using JLAuto.Utils;
using JLAuto.Factory;
using JLAuto.PageObject.JLWeb;
using JLAuto.Infrastructure.Models;
using JLAuto.Infrastructure.Variables;
using JLAuto.DI;

namespace JLAuto.Business.GUI.JLWeb
{
    public class CustomerSearchPage : CustomerPOM<CustomerSearchPage, IModel>
    {
        #region Constructor
        public CustomerSearchPage() : base() { }
        public CustomerSearchPage(DriverFactory driverFactory) : base(driverFactory) { }
        public CustomerSearchPage(DriverFactory driverFactory, TestUtil testUtil) : base(driverFactory, testUtil) { }
        #endregion

        #region Click on Add Customer
        public CustomerSearchPage ClickOnAddCustomer()
        {
            TakeActions(() =>
            {
                waitHelper.WaitForPageFullLoaded();
                keywords.ClickOn(Btn_AddCustomer);
            });
            return this;
        }
        #endregion
    }
}
