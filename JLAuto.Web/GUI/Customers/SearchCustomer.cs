﻿//using Domain.PageObject.JLWeb;
//using Domain.Variables;
//using Infrastructure.Models;
//using Infrastructure.Utils;
using Infrastructure;
using JLAuto.Infrastructure.Variables;
using JLAuto.Business.GUI.JLWeb;
using JLAuto.DI;
using JLAuto.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using BaseTestMsTest = JLAuto.Web.Infrastructure.BaseTestMsTest;

namespace JLAuto.Web.GUI.Customers
{
    [TestClass]
    [TestCategory("Customers")]
    public class SearchCustomer : BaseTestMsTest
    {
        CustomerSearchPage? customerSearchPage;

        [TestMethod]
        public void Demo_JLWeb_GUI_Customers_SearchCustomerByTerms()
        {

        }

        [TestMethod]
        public void Demo_JLWeb_GUI_Customers_SearchCustomer()
        {

        }

        [TestMethod]
        public void Demo_JLWeb_GUI_Customers_SearchCustomerExist()
        {

        }

        [TestInitialize]
        public void NavigateToPage()
        {
            driverFactory?.NavigateToUrl($"{AppConfigFactory.Url}/Customer");
            customerSearchPage = ServicesCollection.Current.Resolve<CustomerSearchPage>();
        }

        [TestCleanup]
        public async Task CustomerCleanup()
        {
            
        }
    }
}
